package com.citm.chatsonya.android.dto.model;

import com.google.gson.annotations.SerializedName;

public class BeritaBaruModel {
 int id;
 String judul_berita,isi_berita,gambar_berita,id_kategori_berita,created_at,updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul_berita() {
        return judul_berita;
    }

    public void setJudul_berita(String judul_berita) {
        this.judul_berita = judul_berita;
    }

    public String getIsi_berita() {
        return isi_berita;
    }

    public void setIsi_berita(String isi_berita) {
        this.isi_berita = isi_berita;
    }

    public String getGambar_berita() {
        return gambar_berita;
    }

    public void setGambar_berita(String gambar_berita) {
        this.gambar_berita = gambar_berita;
    }

    public String getId_kategori_berita() {
        return id_kategori_berita;
    }

    public void setId_kategori_berita(String id_kategori_berita) {
        this.id_kategori_berita = id_kategori_berita;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
