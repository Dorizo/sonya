package com.citm.chatsonya.android.view.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.citm.chatsonya.android.Adapter.MessageAdapter;
import com.citm.chatsonya.android.BaseTabFragment;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.fragment.ChatController;
import com.citm.chatsonya.android.dto.model.Inbox;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.LifeCycleMonitor;
import com.citm.chatsonya.android.helpers.LifeCycleMonitorChat;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.view.chat.UserController;
import com.citm.chatsonya.android.view.chat.showChatController;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Reason;

import java.util.ArrayList;

import javax.inject.Inject;

public class chat extends BaseTabFragment<ChatController> {

    private FloatingActionButton floatingActionButton;
    @Inject
    NavigationManager navigationManager;
    DatabaseHandler databaseHandler;
    MessageAdapter messageAdapter;
    ArrayList<Inbox> arrayinbox = new ArrayList<>();
    RecyclerView recyclerView;
    LinearLayoutManager mLayoutManager;
    Context context;
    SharedPreferences mSettings;
    BroadcastReceiver broadcastReceiver;

    SwipeRefreshLayout mSwipeRefreshLayout;

    @Inject
    private LifeCycleMonitorChat livechat;

    @Override
    protected LifeCycleMonitor getLifeCycleMonitor() {
        return livechat;
    }

    @Override
    protected Class<ChatController> getControllerClass() {
        return ChatController.class;
    }

    @Override
    protected int getLayoutResId() {

        return R.layout.fragmnent_chat;
    }
    @Override
    public void onViewReady(View view, Bundle savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);

        mSwipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayouttimeline);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                update();
            }
        });
        mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
        floatingActionButton=(FloatingActionButton) view.findViewById(R.id.fab);
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            floatingActionButton.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.default_user));
        }else {
            floatingActionButton.setBackground(getActivity().getResources().getDrawable(R.drawable.default_user));
        }
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigationManager.navigate(v).to(showChatController.class);


            }
        });

        setHasOptionsMenu(true);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_beritabaru);
        recyclerView.setHasFixedSize(true);
        setHasOptionsMenu(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        databaseHandler = new DatabaseHandler(getActivity());
        arrayinbox = databaseHandler.getAllMessages();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                update();

            }
        };
    }

    @Override
    public void onResume() {
        update();
        IntentFilter filterRefreshUpdate = new IntentFilter();
        filterRefreshUpdate.addAction(RetrofitInstance.PUSH_GROUP_NOTIFICATION);
        filterRefreshUpdate.addAction(RetrofitInstance.PUSH_NOTIFICATION);

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver,
                filterRefreshUpdate);
        super.onResume();

    }

    @Override
    public void onPause() {

        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.chat_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
             case R.id.adduser:
                 navigationManager.navigate(this).to(UserController.class);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public void update() {
        arrayinbox = databaseHandler.getAllMessages();
        messageAdapter = new MessageAdapter(arrayinbox, getContext(), mSettings.getString("id_user", null), databaseHandler , navigationManager);
        recyclerView.setAdapter(messageAdapter);
        if(arrayinbox != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }else {

            mSwipeRefreshLayout.setRefreshing(true);
        }
        controller.updateToken(mSettings.getString("key" , "messing"));

    }


}
