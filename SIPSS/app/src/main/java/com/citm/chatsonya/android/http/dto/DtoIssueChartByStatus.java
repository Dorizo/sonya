

package com.citm.chatsonya.android.http.dto;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoIssueChartByStatus extends DtoParent implements Serializable {


    @SerializedName("data")
    private DtoIssueChartByStatusData data;
}
