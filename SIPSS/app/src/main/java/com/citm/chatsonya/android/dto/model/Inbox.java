package com.citm.chatsonya.android.dto.model;
public class Inbox {
    String p_message_id;
    String p_sender;
    String p_revicerd;
    String p_message;
    String id_conversation;
    String Message_name;
    String pgroup;

    public Inbox(String p_message_id, String pgroup ,  String p_sender, String p_revicerd, String p_message, String id_conversation , String message_name) {
        this.p_message_id = p_message_id;
        this.p_sender = p_sender;
        this.p_revicerd = p_revicerd;
        this.p_message = p_message;
        this.id_conversation = id_conversation;
        this.Message_name = message_name;
        this.pgroup = pgroup;
    }

    public String getP_message_id() {
        return p_message_id;
    }

    public String getId_conversation() {
        return id_conversation;
    }

    public String getP_sender() {
        return p_sender;
    }

    public String getP_revicerd() {
        return p_revicerd;
    }

    public String getP_message() {
        return p_message;
    }

    public String getMessage_name() {
        return Message_name;
    }

    public String getPgroup() {
        return pgroup;
    }
}
