package com.citm.chatsonya.android.view.group;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.citm.chatsonya.android.Adapter.ChatAdapter;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.activity.group.AnggotaGroup;
import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.model.Message;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.http.AllDataService;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconMultiAutoCompleteTextView;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
import com.shipdream.lib.android.mvc.NavigationManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class groupbox extends AppCompatActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    private static final String TAG = "nulled" ;
    ImageButton btnEmoticons, btnImage;
    LinearLayout btnSend, btnCancel, chatForm;
    EmojiconMultiAutoCompleteTextView txtMessage;
    FrameLayout emojiconLayout;
    RecyclerView ChatView;
    LinearLayoutManager layoutManager;
    SharedPreferences mSettings;
    BroadcastReceiver mgroupReciver;
    Toolbar toolbar;
    Bitmap bitmap;
    boolean isEmojiKeyboardShown = false;
    DatabaseHandler dhelper;
    ChatAdapter chatAdapter;
    ArrayList<Message> messagesList;
    SimpleDateFormat crTime;
    Calendar c;
    String KODE_SAYA,NAMA_GROUP,ID_PERCAKAPAN , KEY;
    AllDataService service;
    @Inject
    NavigationManager navigationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privatechat_layout);
        Log.e(TAG, "onCreate: "+FirebaseInstanceId.getInstance().getToken());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            ((AppCompatActivity)this).setSupportActionBar(toolbar);
        }
        if (this instanceof AppCompatActivity) {
            ((AppCompatActivity)this).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        emojiconLayout= (FrameLayout) findViewById(R.id.emojicons);
        btnEmoticons = (ImageButton) findViewById(R.id.btnEmoticons);
        btnImage = (ImageButton) findViewById(R.id.btnImage);
        btnSend = (LinearLayout) findViewById(R.id.btnSend);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        txtMessage = (EmojiconMultiAutoCompleteTextView) findViewById(R.id.txtMessage);
        chatForm = (LinearLayout) findViewById(R.id.form);
        ChatView = (RecyclerView) findViewById(R.id.viewChat);
        layoutManager = new LinearLayoutManager(getBaseContext());
        dhelper = new DatabaseHandler(getBaseContext());
        ChatView = (RecyclerView) findViewById(R.id.viewChat);
        ChatView.setLayoutManager(layoutManager);
        ChatView.setItemAnimator(new DefaultItemAnimator());
        btnSend.setVisibility(View.GONE);
        crTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        c = Calendar.getInstance();
        mSettings = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
        final Bundle b = getIntent().getExtras();
        if(b!=null){
            messagesList = dhelper.Groupmessage(b.getString("id_gruopconvernce"));
            for (Message messageeeeee : dhelper.Groupmessage(b.getString("id_gruopconvernce"))) {
                Log.e(TAG, "onCreate: "+ messageeeeee.getGroupID()+messageeeeee.getMessage_name() );
            }
            chatAdapter = new ChatAdapter(getBaseContext(),messagesList , b.getString("id"), true, false,"14",null);
            ChatView.setAdapter(chatAdapter);
            ChatView.scrollToPosition(messagesList.size()-1);
            KODE_SAYA = mSettings.getString("id_user" , null);
            NAMA_GROUP = b.getString("toolbartitle");
            ID_PERCAKAPAN = b.getString("id_gruopconvernce");

            toolbar.setTitle("GROUP "+NAMA_GROUP);
        }


     btnEmoticons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEmojiKeyboardShown = !isEmojiKeyboardShown;
                showEmojiconFragment(isEmojiKeyboardShown);
                if (isEmojiKeyboardShown) {
                    btnEmoticons.setImageResource(R.drawable.ic_keyboard);
                    txtMessage.requestFocus();
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(txtMessage.getWindowToken(), 0);
                } else {
                    btnEmoticons.setImageResource(R.drawable.emoticons_button);
                    txtMessage.requestFocus();
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(txtMessage, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        });

        txtMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEmojiconFragment(false);
                btnEmoticons.setImageResource(R.drawable.emoticons_button);
            }
        });

        txtMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtMessage.getText().toString().trim().isEmpty()) {
                    btnSend.setVisibility(View.GONE);
                } else {
                    btnSend.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetMessage();
            }
        });

        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        mgroupReciver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(RetrofitInstance.PUSH_GROUP_NOTIFICATION)){
                    Log.e(TAG, "onReceive: "+intent );
                    messagesList = dhelper.Groupmessage(b.getString("id_gruopconvernce"));
                    chatAdapter.notifyDataSetChanged();
                    scrollToLast();
                }
            }
        };

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strCreation = crTime.format(c.getTime());
                if (txtMessage.getText().toString().trim().isEmpty()) {
                    return;
                }

                Log.e(TAG, "onClick: kirim :"+txtMessage.getText() );

                Message message_send = new Message("1" , KODE_SAYA ,KODE_SAYA , txtMessage.getText().toString() , ID_PERCAKAPAN,strCreation,NAMA_GROUP ,"private");
                dhelper.AddMessage(message_send);
                messagesList.add(message_send);
                chatAdapter.notifyDataSetChanged();
                scrollToLast();
                kirim(mSettings.getString("key" , "missing"),txtMessage.getText().toString(),NAMA_GROUP,KODE_SAYA,KODE_SAYA,ID_PERCAKAPAN);
                resetMessage();
            }
        });


    }

    private void resetMessage() {
        txtMessage.setText(null);
        txtMessage.setEnabled(true);
        btnCancel.setVisibility(View.GONE);
        btnEmoticons.setVisibility(View.VISIBLE);
        btnSend.setVisibility(View.GONE);
        bitmap = null;

    }
    void scrollToLast()
    {
        if (chatAdapter.getItemCount() > 1) {
            ChatView.getLayoutManager().scrollToPosition(chatAdapter.getItemCount() - 1);
        }
    }
    private void kirim(String key , String message , String userselfssss , String SELF , String RECIVER , String id_conversation){

        Call<PostResponse> call = service.groupmessage("Bearer "+ key, message, userselfssss , SELF, RECIVER , id_conversation, "1");
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {

            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {

            }
        });
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mgroupReciver,
                new IntentFilter(RetrofitInstance.PUSH_GROUP_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mgroupReciver);
        super.onPause();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.groupboxmenu , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                super.onBackPressed();
                return true;
            case R.id.keluargroup:
                FirebaseMessaging.getInstance().unsubscribeFromTopic(ID_PERCAKAPAN);
                dhelper.DeleteConversation("1" , ID_PERCAKAPAN);
                chatAdapter.notifyDataSetChanged();

                break;
            case R.id.info_group:
                Intent intent = new Intent(getBaseContext() , AnggotaGroup.class);
                intent.putExtra("id_gruopconvernce" , ID_PERCAKAPAN);
                startActivity(intent);

                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(txtMessage, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(txtMessage);
    }

    private void showEmojiconFragment(boolean isVisible) {
        if (isVisible) {
            getSupportFragmentManager().beginTransaction().replace(R.id.emojicons, EmojiconsFragment.newInstance(false)).commit();
            emojiconLayout.setVisibility(View.VISIBLE);
        }
        else {
            emojiconLayout.setVisibility(View.GONE);
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, 1);
    }

}