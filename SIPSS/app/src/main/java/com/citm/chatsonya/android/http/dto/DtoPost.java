
package com.citm.chatsonya.android.http.dto;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DtoPost extends DtoParent{
    @SerializedName("data")
    private ArrayList<DtoPostData> data;


}
