package com.citm.chatsonya.android.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.DetailBeritaBaruController;
import com.shipdream.lib.android.mvc.MvcFragment;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Reason;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import static android.content.ContentValues.TAG;


public class DetailBeritaBaru extends MvcFragment<DetailBeritaBaruController> {
    Toolbar mToolbar;
    @Inject
    NavigationManager navigationManager;
    ImageView iv;
    WebView wv;
    TextView judul;
    @Override
    protected Class<DetailBeritaBaruController> getControllerClass() {
        return DetailBeritaBaruController.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.detail_berita_baru;
    }



    @Override
    public void  onViewReady(View view, Bundle savedInstanceState, Reason reason) {

        super.onViewReady(view, savedInstanceState, reason);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (mToolbar != null) {
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        }
        if (getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        setHasOptionsMenu(true);
        mToolbar.setTitle(controller.getModel().getJudul_berita());
        iv = (ImageView)view.findViewById(R.id.image_detail);
        wv = (WebView)view.findViewById(R.id.webview_detail);
        judul = (TextView)view.findViewById(R.id.judul_detail_berita);




    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigationManager.navigate(getActivity()).back();
//                navigationManager.navigate(this).back();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void update() {
        Picasso.with(getActivity()).load(getResources().getString(R.string.url_image)+controller.getModel().getGambar_berita()).into(iv);
        Log.e(TAG, "update: "+getResources().getString(R.string.url_image)+controller.getModel().getGambar_berita());
        wv.loadData(controller.getModel().getIsi_berita(),"text/html; charset=UTF-8",null);
        judul.setText(controller.getModel().getJudul_berita());
    }
}
