package com.citm.chatsonya.android.service.internal.database;

import com.orm.SugarRecord;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Catur on 10/29/2016.
 */


@Getter
@Setter
public class DbRefBalai extends SugarRecord {
    String id_online;
    String name;
    String code;
    String gps_lat;
    String gps_lng;
    String gps_radius;
    String user_added;
    String user_modified;
    String date_added;
    String date_modified;

    public DbRefBalai() {
    }

}

