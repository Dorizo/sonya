package com.citm.chatsonya.android.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.model.usersmodel;
import com.citm.chatsonya.android.http.AllDataService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class tambahanggotagroupAdapter extends RecyclerView.Adapter<tambahanggotagroupAdapter.ViewHolder> {

    ArrayList<usersmodel> usersmodels;
    Context context;
    AllDataService allDataService;
    String  id_percakapan ,key;


    public tambahanggotagroupAdapter(ArrayList<usersmodel> usersmodels, Context context, AllDataService allDataService ,String key, String id_percakapan) {
        this.usersmodels = usersmodels;
        this.context = context;
        this.allDataService = allDataService;
        this.id_percakapan = id_percakapan;
        this.key = key;
    }
    @Override
    public tambahanggotagroupAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_anggota_groupbaru,parent,false);
        return new tambahanggotagroupAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(tambahanggotagroupAdapter.ViewHolder holder, final int position) {
        holder.email.setText(usersmodels.get(position).getEmail());
        holder.nama.setText(usersmodels.get(position).getName());
        holder.adduserbaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Call<PostResponse> call =  allDataService.addgroupanggota("Bearer "+ key,id_percakapan , usersmodels.get(position).getId());
              call.enqueue(new Callback<PostResponse>() {
                  @Override
                  public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                      if (response.isSuccessful()){
                          Log.e("xxxxxxxxxxxxxxxxxxxxx", "onResponse: "+response.body().getResult());
                          usersmodels.remove(position);
                          notifyDataSetChanged();
                      }else {
                          Log.e("errorbody", "onResponse: "+response.errorBody().source());
                      }

                  }

                  @Override
                  public void onFailure(Call<PostResponse> call, Throwable t) {

                  }
              });


            }
        });


    }
//    public void updatedata( ArrayList<usersmodel> usersmodelmodel){
//        usersmodels.clear();
//        usersmodelmodel.addAll(usersmodelmodel);
////        notifyDataSetChanged();
//        this.notifyDataSetChanged();
//
//    }

    @Override
    public int getItemCount() {
        Log.d("data jumlah", "getItemCount: "+usersmodels.size());
        return usersmodels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nama , email;
        ImageView adduserbaru;

        public ViewHolder(View itemView) {
            super(itemView);
            nama = (TextView)itemView.findViewById(R.id.namaanggotabaru);
            email = (TextView)itemView.findViewById(R.id.emailanggotabaru);
            adduserbaru = (ImageView)itemView.findViewById(R.id.adduserbaru);
        }
    }
}
