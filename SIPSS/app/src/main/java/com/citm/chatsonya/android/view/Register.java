package com.citm.chatsonya.android.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.citm.chatsonya.android.MainActivity;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.LoginController;
import com.citm.chatsonya.android.controller.RegisterContoller;
import com.citm.chatsonya.android.helpers.daftarclasstambahan;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.sdsmdg.tastytoast.TastyToast;
import com.shipdream.lib.android.mvc.MvcFragment;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Reason;

import javax.inject.Inject;

import static android.content.ContentValues.TAG;


public class Register extends MvcFragment<RegisterContoller> implements daftarclasstambahan {
    Toolbar mToolbar;
    EditText nama,email,password,password_confirmation,telp;
    Button buttons;
    DatabaseHandler db;


    @Inject
    private NavigationManager navigationManager;


    @Override
    protected Class getControllerClass() {
        return RegisterContoller.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_register;
    }


    public void  onViewReady(View view, Bundle savedInstanceState, Reason reason) {

        super.onViewReady(view, savedInstanceState, reason);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (mToolbar != null) {
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        }
        if (getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setHasOptionsMenu(true);
        mToolbar.setTitle("Form Registermodel");
        nama = (EditText) view.findViewById(R.id.nama);
        email = (EditText)view.findViewById(R.id.email);
        password = (EditText)view.findViewById(R.id.password);
        password_confirmation = (EditText)view.findViewById(R.id.password_confirmation);
        buttons = (Button)view.findViewById(R.id.daftarSubmit);
        telp = (EditText)view.findViewById(R.id.telp_daftar) ;
        buttons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.getModel().setEmail(email.getText().toString());
                controller.getModel().setName(nama.getText().toString());
                controller.getModel().setPassword(password.getText().toString());
                controller.getModel().setPassword_confirmation(password_confirmation.getText().toString());
                controller.getModel().setTelp(telp.getText().toString());
                controller.submit();

            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigationManager.navigate(getActivity()).to(LoginController.class);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void update() {

      }


    @Override
    public void kirimtoken() {
        SharedPreferences mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
        Log.e(TAG, "pesan: "+controller.getModel().getToken() );
        if("success".equals(controller.getModel().getPesan())) {
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putString("key" , controller.getModel().getToken());
            editor.putString("user" , controller.getModel().getName());
            editor.putString("id_user" , controller.getModel().getId_user());
            editor.apply();
            TastyToast.makeText(getContext(), "anda Berhasil Login ", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
            Intent ins = new Intent(getContext() , MainActivity.class);
            startActivity(ins);
            getActivity().finish();
        }else{
            TastyToast.makeText(getContext(), controller.getModel().getPesan(), TastyToast.LENGTH_SHORT, TastyToast.WARNING);
        }
        db = new DatabaseHandler(getActivity());
        SQLiteDatabase dbs = db.getWritableDatabase();
        db.onCreate(dbs);
    }
}
