

package com.citm.chatsonya.android.http.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoNotificationData implements Serializable {

    @SerializedName("id")
    public String id_online;
    public String notification_id;
    public String user_id;
    public String title;
    public String content;
    public String route;
    public String is_read;
    public String user_added;
    public String user_modified;
    public String date_added;
    public String date_modified;
    @SerializedName("issue")
    private ArrayList<DtoIssueData> issue;
    public DtoNotificationData(){

    }

}
