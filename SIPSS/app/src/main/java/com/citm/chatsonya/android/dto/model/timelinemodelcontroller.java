package com.citm.chatsonya.android.dto.model;

import java.util.ArrayList;

public class timelinemodelcontroller {

    ArrayList<timelinemodel> timelinemodels;
    ArrayList<usersmodel> usersmodels;

    public ArrayList<usersmodel> getUsersmodels() {
        return usersmodels;
    }

    public void setUsersmodels(ArrayList<usersmodel> usersmodels) {
        this.usersmodels = usersmodels;
    }

    public ArrayList<timelinemodel> getTimelinemodels() {
        return timelinemodels;
    }

    public void setTimelinemodels(ArrayList<timelinemodel> timelinemodels) {
        this.timelinemodels = timelinemodels;
    }
}
