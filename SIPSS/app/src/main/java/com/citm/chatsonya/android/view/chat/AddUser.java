package com.citm.chatsonya.android.view.chat;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.citm.chatsonya.android.Adapter.UserAdapter;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.helpers.activity.LifeUserAdd;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.shipdream.lib.android.mvc.MvcFragment;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Reason;

import javax.inject.Inject;

import ir.oveissi.materialsearchview.MaterialSearchView;

import static android.content.ContentValues.TAG;

public class AddUser extends MvcFragment<UserController> implements LifeUserAdd {
    @Inject
    NavigationManager navigationManager;

    Toolbar mToolbar;
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private Context context;
    SharedPreferences mSettings;
    private UserAdapter userAdapter;
    MaterialSearchView searchView;
    @Override
    protected Class<UserController> getControllerClass() {
        return UserController.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.adduser_layout;
    }


    @Override
    public void  onViewReady(View view, Bundle savedInstanceState, Reason reason) {
        mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
        super.onViewReady(view, savedInstanceState, reason);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        if (mToolbar != null) {
            ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        }
        if (getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setHasOptionsMenu(true);
        mToolbar.setTitle("User");
        recyclerView = (RecyclerView)view.findViewById(R.id.listuser);
         searchView= (MaterialSearchView) view.findViewById(R.id.search_view);
         searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
             @Override
             public boolean onQueryTextSubmit(String s) {
                 Log.e(TAG, "onQueryTextSubmit: onQueryTextSubmit"+s );
                 return false;
             }

             @Override
             public boolean onQueryTextChange(String s) {
//                 Log.e(TAG, "onQueryTextSubmit: onQueryTextChange"+s );
                 controller.retrofituser(mSettings.getString("key","missing") , s);
                 return false;
             }
         });

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        controller.retrofituser(mSettings.getString("key","missing") , null);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.beritabaru_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigationManager.navigate(getActivity()).back();
//                navigationManager.navigate(this).back();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void update() {
        if (controller.getModel().getUserv() !=null){
            Log.e(TAG, "onViewReady: "+controller.getModel().getUserv());
            userAdapter = new UserAdapter(controller.getModel().getUserv(),navigationManager,this , new DatabaseHandler(getActivity()));
            recyclerView.setAdapter(userAdapter);

        }
    }


    @Override
    public void kirimfriendlist(String s) {
        Log.e(TAG, "kirimfriendlist: "+s );
        controller.adduser(mSettings.getString("key","missing"), s);


    }

    @Override
    public void refrestandadduser() {

    }
}
