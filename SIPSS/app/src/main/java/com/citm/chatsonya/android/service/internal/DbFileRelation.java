package com.citm.chatsonya.android.service.internal;

import com.orm.SugarRecord;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Catur on 10/29/2016.
 */


@Getter
@Setter
public class DbFileRelation extends SugarRecord {

    String ref_id;
    String thumbnail;
    String file_path;
    String sync;


    public DbFileRelation() {
    }

    public DbFileRelation(
            String ref_id,
            String file_path,
            String run
    ) {
        if (ref_id != null)
            setRef_id(ref_id);
        else
            setRef_id("");
        if (file_path != null)
            setFile_path(file_path);
        else
            setFile_path("");

        if (run != null)
            setSync(run);
        else
            setSync("0");
    }

    public void edit(
            String ref_id,
            String file_path,
            String run
    ) {

        if (ref_id != null)
            setRef_id(ref_id);
        else
            setRef_id("");
        if (file_path != null)
            setFile_path(file_path);
        else
            setFile_path("");

        if (run != null)
            setSync(run);
        else
            setSync("0");
    }
}

