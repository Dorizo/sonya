package com.citm.chatsonya.android.http;


import com.citm.chatsonya.android.dto.BeritaBaruModelResponse;
import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.RegistermodelResponse;
import com.citm.chatsonya.android.dto.UserResponse;
import com.citm.chatsonya.android.dto.timelineResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


public interface AllDataService {

    @POST("register")
    Call<RegistermodelResponse> register(@Query("image") String gambar, @Query("telp") String telp, @Query("name") String name, @Query("email") String email, @Query("password") String password, @Query("password_confirmation") String password_confirmation, @Query("firebaseid") String firebaseid);

    @POST("login")
    Call<PostResponse> login(@Query("email") String email, @Query("password") String password, @Query("firebaseid") String firebaseid);

    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @GET("berita")
    Call<BeritaBaruModelResponse> berita(@Header("Authorization") String header);
    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @GET("user")
    Call<UserResponse> user(@Header("Authorization") String header, @Query("search") String seacrh);

    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @GET("userfriendlist")
    Call<UserResponse> userfriendlist(@Header("Authorization") String header);

    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @GET("useractivechat")
    Call<UserResponse> useractivechat(@Header("Authorization") String header, @Query("search") String seacrh);

    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @POST("adduser")
    Call<PostResponse> adduser(@Header("Authorization") String header, @Query("tambah") String seacrh);



    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @POST("pesan")
    Call<PostResponse> pesan(@Header("Authorization") String header, @Query("body") String body, @Query("title") String title, @Query("reciver_id") String reciver_id, @Query("sender_id") String sender_id, @Query("id_conversation") String id_conversation, @Query("group") String group ,@Query("kode_fungsi") String kode_fungsi);


    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @POST("usersinggle")
    Call<UserResponse> getsinggleuser(@Header("Authorization") String header, @Query("id_user") String id_user);


    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @POST("timeline")
    Call<timelineResponse> gettimeline(@Header("Authorization") String header, @Query("my_id") String my_id);
    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @POST("timelinepercakapan")
    Call<timelineResponse> gettimelinesingle(@Header("Authorization") String header, @Query("id_conversation") String percakapan);
    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @POST("createtimeline")
    Call<PostResponse> createtimeline(@Header("Authorization") String header, @Query("text_timeline") String text_timeline, @Query("image_timeline") String image_timeline, @Query("color_text") String color_text);

    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @POST("groupmessage")
    Call<PostResponse> groupmessage(@Header("Authorization") String header, @Query("body") String body, @Query("title") String title, @Query("reciver_id") String reciver_id, @Query("sender_id") String sender_id, @Query("id_conversation") String id_conversation, @Query("group") String group);

    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @POST("updatetoken")
    Call<PostResponse> updattoken(@Header("Authorization") String header, @Query("token") String token);

    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @GET("usergroup")
    Call<UserResponse> getusermembergroup(@Header("Authorization") String header, @Query("id_conversation") String id_conversation);
    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @GET("hapusanggotagroup")
    Call<PostResponse> hapusanggotagroup(@Header("Authorization") String header, @Query("id") String id, @Query("id_conversation") String id_conversation);
    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @GET("anggotagroupadd")
    Call<UserResponse> anggotaBaruadd(@Header("Authorization") String header, @Query("id_conversation") String id);
    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @POST("addgroupanggota")
    Call<PostResponse> addgroupanggota(@Header("Authorization") String header, @Query("id_conversation") String id_conversation, @Query("userid") String userid);
    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json"
    })
    @Multipart
    @POST("timelineimage")
    Call<PostResponse> timelineimage(@Header("Authorization") String header, @Part("descripsi") String Deskripsi, @Part MultipartBody.Part file);


}
