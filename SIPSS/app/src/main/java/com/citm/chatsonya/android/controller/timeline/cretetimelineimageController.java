package com.citm.chatsonya.android.controller.timeline;

import android.net.Uri;

import com.citm.chatsonya.android.dto.model.timelinemodel;
import com.shipdream.lib.android.mvc.FragmentController;
import com.shipdream.lib.android.mvc.UiView;

public class cretetimelineimageController extends FragmentController<timelinemodel, UiView> {

    @Override
    public Class<timelinemodel> modelType() {
        return timelinemodel.class;
    }

    public void getimage(Uri url){
        getModel().setImage(url);

    }


}
