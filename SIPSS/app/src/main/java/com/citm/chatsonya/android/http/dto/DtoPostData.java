

package com.citm.chatsonya.android.http.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class DtoPostData implements Serializable {

    public  String id;
}
