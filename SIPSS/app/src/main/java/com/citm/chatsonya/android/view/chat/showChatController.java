package com.citm.chatsonya.android.view.chat;

import android.content.Context;
import android.util.Log;

import com.citm.chatsonya.android.dto.UserResponse;
import com.citm.chatsonya.android.dto.model.userscontrllermodel;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.activity.LifeUserAdd;
import com.citm.chatsonya.android.http.AllDataService;
import com.google.gson.Gson;
import com.shipdream.lib.android.mvc.FragmentController;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class showChatController extends FragmentController<userscontrllermodel, LifeUserAdd> {


    Context context;
    AllDataService service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
    @Override
    public Class<userscontrllermodel> modelType() {
        return userscontrllermodel.class;
    }
    public void retrofituser(String key , String serach){
        Call<UserResponse> call = service.useractivechat("Bearer "+key , serach);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()){
                    getModel().setUserv(response.body().getResult());
                    view.update();
                }else {
                    Gson gs = new Gson();
                    Log.e(TAG, "error onResponse: "+ gs.toJson(response.errorBody()));

                }
            }
            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

            }
        });

    }

    public void refreshretrofituserfriendlist(String key){
        Call<UserResponse> call = service.userfriendlist("Bearer "+key);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()) {
                    getModel().setUserlist(response.body().getResult());
                    view.refrestandadduser();
                    Log.e(TAG, "onResponse: "+response.body().getResult().size());
                }else {
                    Log.e(TAG, "onResponse: "+response.errorBody().source() );

                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

            }
        });


    }





}
