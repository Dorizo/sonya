package com.citm.chatsonya.android.factory;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {
    public static Retrofit retrofit;
//    private static final String BASE_URL = "http://navjacinth9.000webhostapp.com/json/";

    private static final String BASE_URL = "http://192.168.1.228/chatsonya/public/api/";
    public static final String Image_url = "http://192.168.1.228/chatsonya/public/";
//    private static final String BASE_URL = "http://192.168.3.221/chatsonya/public/api/";
//    private static final String BASE_URL = "http://192.168.2.25/chatsonya/public/api/";
//    private static final String BASE_URL = "http://192.168.2.17/chatsonya/public/api/";
//    private static final String BASE_URL = "http://192.168.43.2/chatsonya/public/api/";
//    private static final String BASE_URL = "http://192.168.100.153/chatsonya/public/api/";
//    private static final String BASE_URL = "http://sonya.ciptateknologimuda.com/public/api/";

    public static final String PUSH_NOTIFICATION = "push";
    public static final String PUSH_GROUP_NOTIFICATION = "group_push";

    public static Retrofit RetrofitInstance(){
        if (retrofit==null){
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

        }
        return retrofit;
    }
}
