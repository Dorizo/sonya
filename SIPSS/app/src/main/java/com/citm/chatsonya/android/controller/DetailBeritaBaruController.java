package com.citm.chatsonya.android.controller;

import android.util.Log;

import com.citm.chatsonya.android.dto.model.BeritaBaruModel;
import com.google.gson.Gson;
import com.shipdream.lib.android.mvc.FragmentController;
import com.shipdream.lib.android.mvc.UiView;

import static android.content.ContentValues.TAG;

public class DetailBeritaBaruController extends FragmentController<BeritaBaruModel, UiView> {

    @Override
    public Class<BeritaBaruModel> modelType() {
        return BeritaBaruModel.class;
    }

    public void x(String  dorizo){
        Gson gs = new Gson();
        BeritaBaruModel br =gs.fromJson(dorizo,BeritaBaruModel.class);
        Log.e(TAG, "x: "+  br.getGambar_berita());
        getModel().setJudul_berita(br.getJudul_berita());
        getModel().setGambar_berita(br.getGambar_berita());
        getModel().setIsi_berita(br.getIsi_berita());
    }
}
