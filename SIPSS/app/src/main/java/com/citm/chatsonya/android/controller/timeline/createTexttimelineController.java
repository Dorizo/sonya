package com.citm.chatsonya.android.controller.timeline;

import android.content.Context;
import android.util.Log;

import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.model.timelinemodel;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.http.AllDataService;
import com.shipdream.lib.android.mvc.FragmentController;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.UiView;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class createTexttimelineController extends FragmentController<timelinemodel, UiView> {
    @Inject
    NavigationManager navigationManager;
    Context context;
    AllDataService service;
    @Override
    public Class<timelinemodel> modelType() {
        return timelinemodel.class;
    }

    public void create(String key , String text_timeline){
        service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
        Call<PostResponse> call = service.createtimeline("Bearer "+key , text_timeline,"default.jpg" , "#FF00FF");
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                if(response.isSuccessful()){
                    view.update();
                    navigationManager.navigate(context).back();
                }else {
                    navigationManager.navigate(context).back();
                    view.update();
                }
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );

            }
        });

    }
}
