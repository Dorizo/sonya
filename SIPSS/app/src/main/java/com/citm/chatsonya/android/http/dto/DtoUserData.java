

package com.citm.chatsonya.android.http.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoUserData  implements Serializable {

    @SerializedName("id")
    public String id_online;
    public String package_id;
    public String package_name;
    public String account_type;
    public String soc_id;
    public String username;
    public String firstname;
    public String lastname;
    public String biografi;
    public String alamat;
    public String photo_thumb_url;
    public String phone;
    public String access;
    public String email;
    public String token;
    public String password;
    public String photo;
    public String config_jenjang;
    public String config_class;
    public String religion;
    public String religion_lesson;
    public DtoUserData(){

    }
}
