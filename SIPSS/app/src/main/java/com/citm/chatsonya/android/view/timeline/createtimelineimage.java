package com.citm.chatsonya.android.view.timeline;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.chartActifity.FilePath;
import com.citm.chatsonya.android.controller.timeline.cretetimelineimageController;
import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.model.Post;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.http.AllDataService;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.shipdream.lib.android.mvc.MvcFragment;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Reason;

import java.io.File;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static net.alhazmy13.mediapicker.Image.ImageTags.Tags.TAG;

public class createtimelineimage extends MvcFragment<cretetimelineimageController> {
    Toolbar mToolbar;
    ImageView imageView;
    AllDataService service;
    ImageButton imageButton;
    TextView balasstatusimage;
    String filepath;
    SharedPreferences mSettings;
    @Inject
    NavigationManager navigationManager;
    Context context;
    Gson gson;
//    AllDataService serviceer;

    @Override
    protected Class<cretetimelineimageController> getControllerClass() {
        return cretetimelineimageController.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.timelineimage;
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);
        checkAndRequestPermissions();

        Fabric.with(getContext(), new Crashlytics());

        gson = new Gson();
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        imageView = (ImageView)view.findViewById(R.id.imageViewTimelinedetail);
        imageButton = (ImageButton)view.findViewById(R.id.statusimagedetailtimeline);
        balasstatusimage = (TextView)view.findViewById(R.id.balasstatusimage);

        mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
        if (mToolbar != null) {
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        }
        if (getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setHasOptionsMenu(true);

        mToolbar.setTitle("Buat Timeline");
        service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

//                Crashlytics.getInstance().crash();
                filepath = FilePath.getPath(getContext(), controller.getModel().getImage());
                try {
                    File fileimage = new File(filepath);

                    Log.d(TAG, "Filename " + fileimage.getName());
                    RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), fileimage);
                    MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", fileimage.getName(), mFile);
                    Log.d(TAG, "requestBody " +mFile);


                Call<PostResponse> call =  service.timelineimage("Bearer " + mSettings.getString("key", null),balasstatusimage.getText().toString(),fileToUpload);
                call.enqueue(new Callback<PostResponse>() {
                    @Override
                    public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
//                        Log.e(TAG, "onResponse: "+response);
                        if (response.isSuccessful()){
                            Post post = response.body().getResult().get(0);
                            String s = gson.toJson(response.body());


                            Log.d(TAG, "onResponse: "+s);

                            navigationManager.navigate(getActivity()).back();

                        }else{
                            Log.e(TAG, "onResponse error body: "+response.errorBody().source());
                        }
                    }

                    @Override
                    public void onFailure(Call<PostResponse> call, Throwable t) {
                        Log.e(TAG, "onResponse error: "+t );
                    }
                });
//                Log.e(TAG, "onClick: "+filepath);
                }catch (Exception e){
                    Log.e(TAG, "onClick: errer "+e.getMessage() );

                }

            }

        });



    }

    private boolean checkAndRequestPermissions() {
        if (ActivityCompat.checkSelfPermission(getActivity().getBaseContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION
            }, 10);

        };

        if (ActivityCompat.checkSelfPermission(getActivity().getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE
            }, 100);

        };
        if (ActivityCompat.checkSelfPermission(getActivity().getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 112);

        };


        return true;
    }



    @Override
    public void update() {
        controller.getModel().getImage();
        imageView.setImageURI(controller.getModel().getImage());

    }


}
