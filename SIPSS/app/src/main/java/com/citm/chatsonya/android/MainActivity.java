package com.citm.chatsonya.android;

import android.content.Context;
import android.content.SharedPreferences;

import com.citm.chatsonya.android.controller.DetailBeritaBaruController;
import com.citm.chatsonya.android.controller.HomeController;
import com.citm.chatsonya.android.controller.LoginController;
import com.citm.chatsonya.android.controller.RegisterContoller;
import com.citm.chatsonya.android.controller.group.groupBaruController;
import com.citm.chatsonya.android.controller.splashcontroller;
import com.citm.chatsonya.android.controller.timeline.TimelineDetailController;
import com.citm.chatsonya.android.controller.timeline.createTexttimelineController;
import com.citm.chatsonya.android.controller.timeline.cretetimelineimageController;
import com.citm.chatsonya.android.view.DetailBeritaBaru;
import com.citm.chatsonya.android.view.Login;
import com.citm.chatsonya.android.view.Register;
import com.citm.chatsonya.android.view.chat.AddUser;
import com.citm.chatsonya.android.view.chat.UserController;
import com.citm.chatsonya.android.view.chat.showChat;
import com.citm.chatsonya.android.view.chat.showChatController;
import com.citm.chatsonya.android.view.group.groupBaru;
import com.citm.chatsonya.android.view.timeline.TimelineDetail;
import com.citm.chatsonya.android.view.timeline.createTexttimeline;
import com.citm.chatsonya.android.view.timeline.createtimelineimage;
import com.citm.chatsonya.android.view.viewSplashscreen;
import com.shipdream.lib.android.mvc.Forwarder;
import com.shipdream.lib.android.mvc.FragmentController;
import com.shipdream.lib.android.mvc.MvcFragment;
import com.shipdream.lib.android.mvc.NavigationManager;

import javax.inject.Inject;

public class MainActivity extends chatsonya {

    @Override
    protected Class<? extends MvcFragment> mapFragmentRouting(Class<? extends FragmentController> controllerClass) {

        if (controllerClass == splashcontroller.class) {
            return viewSplashscreen.class;
        }else if(controllerClass == RegisterContoller.class){
            return Register.class;
        }else if(controllerClass == LoginController.class){
            return Login.class;
        }else if(controllerClass == DetailBeritaBaruController.class){
            return DetailBeritaBaru.class;
        }else if(controllerClass == UserController.class){
            return AddUser.class;
        }else if(controllerClass == showChatController.class){
            return showChat.class;
        }else if(controllerClass == TimelineDetailController.class){
            return TimelineDetail.class;
        }else if(controllerClass==createTexttimelineController.class){
            return createTexttimeline.class;
        }else if(controllerClass==groupBaruController.class){
            return groupBaru.class;
        }else if(controllerClass == cretetimelineimageController.class){
            return createtimelineimage.class;
        }
        return viewSplashscreen.class;

    }

    @Override
    protected Class<? extends DelegateFragment> getDelegateFragmentClass() {
        return HomeFragment.class;
    }
    public static class HomeFragment extends DelegateFragment<HomeController> {
        @Inject
        private NavigationManager navigationManager;
        @Override
        protected void onStartUp() {
            SharedPreferences mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
            String C = mSettings.getString("key" , "missing");
            if(C.equals("missing")){
                navigationManager.navigate(this).to(LoginController.class, new Forwarder().clearAll());
            }else{
                navigationManager.navigate(this).to(splashcontroller.class, new Forwarder().clearAll());
            }

        }

        @Override
        protected Class<HomeController> getControllerClass() {
            return HomeController.class;
        }




        @Override
        public void update() {

        }
    }


}
