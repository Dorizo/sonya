package com.citm.chatsonya.android.controller;

import android.util.Log;

import com.citm.chatsonya.android.dto.RegistermodelResponse;
import com.citm.chatsonya.android.dto.model.Registermodel;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.daftarclasstambahan;
import com.citm.chatsonya.android.http.AllDataService;
import com.google.firebase.iid.FirebaseInstanceId;
import com.shipdream.lib.android.mvc.FragmentController;
import com.shipdream.lib.android.mvc.Reason;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class RegisterContoller extends FragmentController<Registermodel, daftarclasstambahan> {
    @Override
    public Class<Registermodel> modelType() {
        return Registermodel.class;
    }
    public void setPesan(String s){
        getModel().setPesan(s);
    }
    @Override
    public void onViewReady(Reason reason) {
        super.onViewReady(reason);
        view.update();
        
        if (reason.isRestored()) {

        }
    }


    public void submit(){
        AllDataService service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
         Call<RegistermodelResponse> call = service.register("avatar.jpg",getModel().getTelp(),getModel().getName(),getModel().getEmail(),getModel().getPassword(),getModel().getPassword_confirmation() , refreshedToken);
          call.enqueue(new Callback<RegistermodelResponse>() {
          @Override
          public void onResponse(Call<RegistermodelResponse> call, Response<RegistermodelResponse> response) {
              if(response.isSuccessful()) {
                  Registermodel kode = response.body().getResult().get(0);
                  getModel().setName(kode.getName());
                  getModel().setEmail(kode.getEmail());
                  getModel().setPassword(kode.getPassword());
                  getModel().setPassword_confirmation(kode.getPassword_confirmation());

                  Log.e(TAG, "onResponse: "+kode.getName());
                  setPesan(kode.getName().toString());
                  if(response.code() == 200){
                  getModel().setToken(kode.getToken().toString());
                  getModel().setId_user(kode.getId_user().toString());
                  }

                  view.kirimtoken();


              }else {
                  Log.e(TAG, "onResponse error: " + response.errorBody().source());
                 setPesan(response.errorBody().source().toString());

              }
              view.update();

          }
          @Override
          public void onFailure(Call<RegistermodelResponse> call, Throwable t) {
              Log.e(TAG, "onFailure: "+t );

          }
      });


    }

    @Override
    public Registermodel getModel() {
        return super.getModel();
    }


}
