package com.citm.chatsonya.android.dto.model;

import java.util.ArrayList;

public class timelineControllerModel {
    ArrayList<timelinemodel> timelinemodels;

    public ArrayList<timelinemodel> getTimelinemodels() {
        return timelinemodels;
    }

    public void setTimelinemodels(ArrayList<timelinemodel> timelinemodels) {
        this.timelinemodels = timelinemodels;
    }
}
