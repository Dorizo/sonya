package com.citm.chatsonya.android.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.citm.chatsonya.android.MainActivity;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.LoginController;
import com.citm.chatsonya.android.controller.RegisterContoller;
import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.AsyncView;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.http.AllDataService;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sdsmdg.tastytoast.TastyToast;
import com.shipdream.lib.android.mvc.MvcFragment;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Preparer;
import com.shipdream.lib.android.mvc.Reason;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class Login extends MvcFragment<LoginController> implements AsyncView {
    Toolbar mToolbar;
    EditText email,password;
    Button Login , Register;
    @Inject
    NavigationManager navigationManager;
    AllDataService service;

    DatabaseHandler db;
    @Override
    protected Class<LoginController> getControllerClass() {
        return LoginController.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_login;
    }

    @Override
    public void  onViewReady(View view, Bundle savedInstanceState, Reason reason) {

        super.onViewReady(view, savedInstanceState, reason);
        db = new DatabaseHandler(getActivity());
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (mToolbar != null) {
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        }
        mToolbar.setTitle("Sonya Login");
        email = (EditText)view.findViewById(R.id.email);
        password = (EditText)view.findViewById(R.id.password);
        Login = (Button)view.findViewById(R.id.Login);
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.login(email.getText().toString(),password.getText().toString());
            }
        });
        Register = (Button)view.findViewById(R.id.Register_button);
        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    try {

                        navigationManager.navigate(getContext()).with(RegisterContoller.class, new Preparer<RegisterContoller>() {
                            @Override
                            public void prepare(RegisterContoller controller_to) {

                            }
                        }).to(RegisterContoller.class);

                    }catch (Exception E){

                    }



            }
        });
        service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);


    }

    @Override
    public void update() {

    }


    @Override
    public void showLoadingStatus() {

    }

    @Override
    public void hideLoadingStatus() {

    }

    @Override
    public void loginsubmit(String user , String key , String iduser) {
        SharedPreferences mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
        if(key.equals("kosong")){
            TastyToast.makeText(getContext(),user,TastyToast.LENGTH_SHORT,TastyToast.ERROR);

        }else{
            updateToken(key);
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putString("key" , key);
            editor.putString("user" , user);
            editor.putString("id_user" , iduser);
            editor.apply();
            TastyToast.makeText(getContext(), "anda Berhasil Login", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
            Intent ins = new Intent(getContext() , MainActivity.class);
            startActivity(ins);
            getActivity().finish();
        }
        SQLiteDatabase dbs = db.getWritableDatabase();
        db.onCreate(dbs);

    }

    public void updateToken(String key){

        Call<PostResponse> call = service.updattoken("Bearer "+ key , FirebaseInstanceId.getInstance().getToken());
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                Log.e(TAG, "onResponse: "+response.code() );

            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );

            }
        });



    }

}
