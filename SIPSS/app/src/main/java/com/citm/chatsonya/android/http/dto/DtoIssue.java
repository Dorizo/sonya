

package com.citm.chatsonya.android.http.dto;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoIssue extends DtoParent implements Serializable {

    @SerializedName("data")
    private ArrayList<DtoIssueData> data;
}
