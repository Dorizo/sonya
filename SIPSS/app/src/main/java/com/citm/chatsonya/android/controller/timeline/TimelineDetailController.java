package com.citm.chatsonya.android.controller.timeline;

import android.util.Log;

import com.citm.chatsonya.android.dto.model.timelinemodel;
import com.google.gson.Gson;
import com.shipdream.lib.android.mvc.FragmentController;
import com.shipdream.lib.android.mvc.UiView;

import static android.content.ContentValues.TAG;

public class TimelineDetailController extends FragmentController<timelinemodel , UiView> {
    @Override
    public Class<timelinemodel> modelType() {
        return timelinemodel.class;
    }

    public void daritimelineadapter(String Alldata){

        Log.e(TAG, "daritimelineadapter: "+Alldata );
        Gson gson = new Gson();
        timelinemodel tm = gson.fromJson(Alldata , timelinemodel.class);
        getModel().setColor_text(tm.getColor_text());
        getModel().setCreated_at(tm.getCreated_at());
        getModel().setImage_timeline(tm.getImage_timeline());
        getModel().setName(tm.getName());
        getModel().setId(tm.getId());
        getModel().setText_timeline(tm.getText_timeline());
        getModel().setUsers_id(tm.getUsers_id());





    }


}
