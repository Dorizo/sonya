package com.citm.chatsonya.android.view.chat;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.citm.chatsonya.android.Adapter.showChatAdapter;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.group.groupBaruController;
import com.citm.chatsonya.android.dto.model.usersmodel;
import com.citm.chatsonya.android.helpers.activity.LifeUserAdd;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.google.gson.Gson;
import com.sdsmdg.tastytoast.TastyToast;
import com.shipdream.lib.android.mvc.MvcFragment;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Reason;

import javax.inject.Inject;

import ir.oveissi.materialsearchview.MaterialSearchView;

import static android.content.ContentValues.TAG;

public class showChat extends MvcFragment<showChatController> implements LifeUserAdd {
    @Inject
    NavigationManager navigationManager;

    @Override
    protected Class<showChatController> getControllerClass() {
        return showChatController.class;
    }
    Toolbar mToolbar;
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private Context context;
    SharedPreferences mSettings;
    private showChatAdapter userAdapter;
    MaterialSearchView searchView;
    DatabaseHandler db;
    LinearLayout buttonaddgroup;
    @Override
    protected int getLayoutResId() {
        return R.layout.showchat_layout;
    }


    @Override
    public void  onViewReady(View view, Bundle savedInstanceState, Reason reason) {
        mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
        super.onViewReady(view, savedInstanceState, reason);
        db = new DatabaseHandler(getActivity());
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (mToolbar != null) {
            ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        }
        if (getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setHasOptionsMenu(true);
        mToolbar.setTitle("Chat user List");
        recyclerView = (RecyclerView)view.findViewById(R.id.listuser_showchat);
        searchView= (MaterialSearchView) view.findViewById(R.id.search_view_showchat);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.e(TAG, "onQueryTextSubmit: onQueryTextSubmit"+s );
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
//                 Log.e(TAG, "onQueryTextSubmit: onQueryTextChange"+s );
                controller.retrofituser(mSettings.getString("key","missing") , s);
                return false;
            }
        });
        buttonaddgroup = (LinearLayout) view.findViewById(R.id.group_addbotton);
        buttonaddgroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "onClick: "+view );
                navigationManager.navigate(this).to(groupBaruController.class);

            }
        });
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        controller.retrofituser(mSettings.getString("key","missing") , null);
        setHasOptionsMenu(true);
        Gson gs = new Gson();
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.showchat_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
//
//        MenuItem item = menu.findItem(R.id.action_search);
//        searchView.setMenuItem(item);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigationManager.navigate(getActivity()).back();
//                navigationManager.navigate(this).back();
                return true;
                case  R.id.refresh_user:
                controller.refreshretrofituserfriendlist(mSettings.getString("key","missing"));
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void update() {

        if (db.getAllUsers() !=null) {
            userAdapter = new showChatAdapter(db.getAllUsers(), navigationManager, this , db);
            recyclerView.setAdapter(userAdapter);
        }




    }

    @Override
    public void kirimfriendlist(String user_two) {

    }

    @Override
    public void refrestandadduser() {
        // refresh user dan tambah jika tidak ada user
        try{
            for (usersmodel user : controller.getModel().getUserlist()){
                if(!db.isUserExists(user.getId())){
                    Log.e(TAG, "refrestandadduser: "+user.getName() );
                    db.AddUser(user);
                    update();
                }
            }

        }catch (Exception e){
            TastyToast.makeText(context , "user kosong" ,TastyToast.LENGTH_LONG,TastyToast.WARNING);
        }

    }
}
