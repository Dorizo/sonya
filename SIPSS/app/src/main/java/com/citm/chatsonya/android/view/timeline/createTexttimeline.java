package com.citm.chatsonya.android.view.timeline;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.timeline.createTexttimelineController;
import com.shipdream.lib.android.mvc.MvcFragment;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Reason;

import javax.inject.Inject;

public class createTexttimeline  extends MvcFragment<createTexttimelineController>{
    EditText editText;
    FloatingActionButton floatingActionButton;
    SharedPreferences mSettings;
    Toolbar mToolbar;
    @Inject
    NavigationManager navigationManager;
    @Override
    protected Class<createTexttimelineController> getControllerClass() {
        return createTexttimelineController.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.timelinecreatetext;
    }

    @Override
    protected void onViewReady(View view, Bundle savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);
        floatingActionButton = (FloatingActionButton)view.findViewById(R.id.statusimage);
        editText = (EditText)view.findViewById(R.id.textViewtimelinedetail);
        mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);


        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.create(mSettings.getString("key","missing") , editText.getText().toString());
            }
        });

        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (mToolbar != null) {
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        }
        if (getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setHasOptionsMenu(true);
        mToolbar.setTitle("Buat Timeline");
    }

    @Override
    public void update() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigationManager.navigate(getActivity()).back();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
