

package com.citm.chatsonya.android.http.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoInspectionStaData implements Serializable {

    @SerializedName("id")
    public String id_online;
    public String id_local;
    public String project_name;
    public String river_name;
    public String inspection_river_id;
    public String name;
    public String description;
    public String gps_lat;
    public String gps_lng;
    public String kota;
    public String kecamatan;
    public String desa;
    public String dusun;
    public String photo;
    public String photo_kiri;
    public String photo_tengah;
    public String photo_kanan;
    public String user_added;
    public String user_modified;
    public String date_added;
    public String date_modified;
    public DtoInspectionStaData(){

    }

}
