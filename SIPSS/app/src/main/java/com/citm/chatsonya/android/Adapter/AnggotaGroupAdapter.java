package com.citm.chatsonya.android.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.model.Accessadmin;
import com.citm.chatsonya.android.dto.model.usersmodel;
import com.citm.chatsonya.android.http.AllDataService;

import net.alhazmy13.mediapicker.Image.ImageTags;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class AnggotaGroupAdapter extends RecyclerView.Adapter<AnggotaGroupAdapter.ViewHolder> {

    ArrayList<usersmodel> usersmodels;
    Context context;
    AllDataService allDataService;
    String key , id_percakapan;
    Accessadmin accessadmins;


    public AnggotaGroupAdapter(ArrayList<usersmodel> usersmodels, Context context, AllDataService allDataService, String key, String id_percakapan, Accessadmin accessadmins) {
        this.usersmodels = usersmodels;
        this.context = context;
        this.allDataService = allDataService;
        this.key = key;
        this.id_percakapan = id_percakapan;
        this.accessadmins = accessadmins;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_anggotagroup,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.nama.setText(usersmodels.get(position).getName());
        holder.email.setText(usersmodels.get(position).getTelp());
        try {

            //menampilkan tombol delete anggota group
            if (accessadmins.getStatus_acces().equals("1")) {
                holder.hideshow.setVisibility(View.VISIBLE);


            } else {
                holder.hideshow.setVisibility(View.GONE);
            }
        }catch (Exception e){

        }


        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Call<PostResponse> call = allDataService.hapusanggotagroup("Bearer "+ key,usersmodels.get(position).getId() , id_percakapan);
                call.enqueue(new Callback<PostResponse>() {
                    @Override
                    public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                        if(response.isSuccessful()){
                            Log.d(TAG, "onResponse: "+response.body().toString());
                            kirim(key,"group dihapus untuk "+usersmodels.get(position).getName(),usersmodels.get(position).getName(),usersmodels.get(position).getId(),usersmodels.get(position).getId(),id_percakapan);

                        }else {
                            Log.e(TAG, "onResponse: "+response.errorBody().source() );
                        }
                        Log.e(TAG, "onClick: "+response.message() );


                    }

                    @Override
                    public void onFailure(Call<PostResponse> call, Throwable t) {

                    }
                });

                usersmodels.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        Log.e(TAG, "getItemCount: "+usersmodels.size() );
        return usersmodels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nama , email;
        ImageView imageView;
        LinearLayout hideshow;
        public ViewHolder(View itemView) {
            super(itemView);
            nama = (TextView)itemView.findViewById(R.id.nama_user);
            email = (TextView)itemView.findViewById(R.id.email_user);
            imageView = (ImageView)itemView.findViewById(R.id.useranggotadelete);
            hideshow = (LinearLayout)itemView.findViewById(R.id.hideshowdelete);
        }
    }


    private void kirim(String key , String message , String userselfssss , String SELF , String RECIVER , String id_conversation){

        Call<PostResponse> call = allDataService.pesan("Bearer "+ key, message, userselfssss , SELF, RECIVER , id_conversation, "401" ,"private");
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {

                if (response.isSuccessful()){
                    Log.e(ImageTags.Tags.TAG, "onResponse: "+response.body().toString() );
                }else{
                    Log.e(ImageTags.Tags.TAG, "onResponse: "+response.errorBody().source() );
                }

            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                Log.e(ImageTags.Tags.TAG, "onFailure: "+t );

            }
        });
    }
}
