package com.citm.chatsonya.android.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.activity.privatechat.privateChat;
import com.citm.chatsonya.android.dto.model.Inbox;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.view.group.groupbox;
import com.google.gson.Gson;
import com.sdsmdg.tastytoast.TastyToast;
import com.shipdream.lib.android.mvc.NavigationManager;

import java.util.ArrayList;

import javax.inject.Inject;

import static android.content.ContentValues.TAG;


/**
 * This class binds and controls the events related to the elements from rsc_chat_canvas_xxx
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private ArrayList<Inbox> datalist;
    Context context;
    String myiduser;
    Gson gson;
    DatabaseHandler databaseHandler;
    @Inject
    NavigationManager navigationManager;

    public MessageAdapter(ArrayList<Inbox> datalist, Context context, String myiduser,DatabaseHandler databaseHandler , NavigationManager navigationManager) {
        this.datalist = datalist;
        this.context = context;
        this.myiduser = myiduser;
        this.databaseHandler = databaseHandler;
        this.navigationManager = navigationManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        try {


        gson = new Gson();

        String kode = null;
        holder.lastmessage.setText(datalist.get(position).getP_message());
        Log.e(TAG, "onBindViewHolder: "+datalist.get(position).getId_conversation());
        if(myiduser.equals(datalist.get(position).getP_sender())){
            kode = datalist.get(position).getP_revicerd();
        }else {
            kode  = datalist.get(position).getP_sender();
        }
        holder.textnama.setText(datalist.get(position).getMessage_name());
        final String finalKode = kode;
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(datalist.get(position).getPgroup().equals("-1")){
                    Gson gson = new Gson();
                    String gri = "-1";
                    Intent intent = new Intent(view.getContext(), privateChat.class);
                    intent.putExtra("isGroup", gri.equals("-1") ? "0" : "1");
                    intent.putExtra("group_id", gri);
                    intent.putExtra("id", finalKode);
                    intent.putExtra("id_gruopconvernce" , datalist.get(position).getId_conversation());
                    intent.putExtra("username",  databaseHandler.getuser(finalKode));
                    view.getContext().startActivity(intent);
                }else{
                    Intent intents = new Intent(view.getContext() ,groupbox.class);
                    intents.putExtra("isGroup" , datalist.get(position).getPgroup());
                    intents.putExtra("id", finalKode);
                    intents.putExtra("id_gruopconvernce" , datalist.get(position).getId_conversation());
                    intents.putExtra("toolbartitle" , datalist.get(position).getMessage_name());
                    view.getContext().startActivity(intents);
                }




            }
        });
        holder.linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle("Delete Pesan");
                builder.setMessage("Apakah Anda ingin menghapus pesan ini ?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        Log.e(TAG, "hapus data: "+datalist.get(position).getPgroup() );
                        databaseHandler.DeleteConversation(datalist.get(position).getPgroup() , datalist.get(position).getId_conversation());
                        datalist.remove(position);
                        notifyDataSetChanged();

                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

                return true;
            }
        });
        }catch (Exception e){
            TastyToast.makeText(context , e.getMessage() ,TastyToast.LENGTH_LONG,TastyToast.ERROR);

        }
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textnama,lastmessage;
        LinearLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            textnama = (TextView) itemView.findViewById(R.id.nama_user);
            lastmessage = (TextView) itemView.findViewById(R.id.lastmessage);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.pindahprivatemessage);
        }

    }
}