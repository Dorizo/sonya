package com.citm.chatsonya.android.controller.fragment;

import android.util.Log;

import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.model.TabModel;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.http.AllDataService;
import com.google.firebase.iid.FirebaseInstanceId;
import com.shipdream.lib.android.mvc.FragmentController;
import com.shipdream.lib.android.mvc.Reason;
import com.shipdream.lib.android.mvc.UiView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class ChatController extends FragmentController<TabModel, UiView> {
    static final String INIT_TEXT = "Chat Kosong";
    static final String RESTORE_TEXT = "Restored TabA";
    AllDataService service;
    @Override
    public Class<TabModel> modelType() {
        return TabModel.class;
    }
    public void setName(String name){
        getModel().setName(name);
    }
    @Override
    public void onViewReady(Reason reason) {
        super.onViewReady(reason);
        if (reason.isFirstTime()) {
            getModel().setName(INIT_TEXT);
        }
        if (reason.isRestored()) {
            getModel().setName(RESTORE_TEXT);
        }
        service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
    }

    @Override
    public TabModel getModel() {
        return super.getModel();
    }

    @Override
    public void restoreModel(TabModel restoredModel) {
        super.restoreModel(restoredModel);
    }

    @Override
    public void bindModel(TabModel tabModel) {
        super.bindModel(tabModel);
    }

    @Override
    public void onRestored() {
        super.onRestored();
    }

    public void updateToken(String key){

            Call<PostResponse> call = service.updattoken("Bearer " + key, FirebaseInstanceId.getInstance().getToken());
            call.enqueue(new Callback<PostResponse>() {
                @Override
                public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
//                    Log.e(TAG, "onResponse: update key " + response.toString());

                }

                @Override
                public void onFailure(Call<PostResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: update key" + t);

                }
            });




    }
}
