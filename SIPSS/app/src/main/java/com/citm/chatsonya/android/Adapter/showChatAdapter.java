package com.citm.chatsonya.android.Adapter;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.activity.privatechat.privateChat;
import com.citm.chatsonya.android.dto.model.usersmodel;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.view.chat.showChat;
import com.sdsmdg.tastytoast.TastyToast;
import com.shipdream.lib.android.mvc.NavigationManager;

import java.util.List;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;

public class showChatAdapter extends RecyclerView.Adapter<showChatAdapter.userViewHolder> {

    private List<usersmodel> datalist;
    @Inject
    NavigationManager navigationManager;
    showChat context;
    DatabaseHandler db;

    public showChatAdapter(List<usersmodel> datalist, NavigationManager navigationManager, showChat context , DatabaseHandler db) {
        this.datalist = datalist;
        this.navigationManager = navigationManager;
        this.context = context;
        this.db = db;
    }

    @NonNull
    @Override
    public userViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_showcart,parent,false);
        return new userViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final userViewHolder holder, final int position) {
        holder.textemail.setText(datalist.get(position).getEmail());
        holder.textnama.setText(datalist.get(position).getName());

        holder.imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String gri = "-1";
                Intent intent = new Intent(v.getContext(), privateChat.class);
                intent.putExtra("isGroup", gri.equals("-1") ? "0" : "1");
                intent.putExtra("group_id", gri);
                intent.putExtra("username",datalist.get(position).getName());
                intent.putExtra("id" , datalist.get(position).getId());
                v.getContext().startActivity(intent);
                TastyToast.makeText(context.getContext() , "fab bisa ngak ya" , TastyToast.LENGTH_SHORT,TastyToast.SUCCESS);
            }
        });
        holder.linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                 AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle("Delete User");
                builder.setMessage("Apakah Anda ingin menghapus user ini ?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        db.deleteuser(datalist.get(position).getId());
                        datalist.remove(position);
                        notifyDataSetChanged();

                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class userViewHolder extends RecyclerView.ViewHolder {
        TextView textnama, textemail;
        CircleImageView circleImageView;
        LinearLayout linearLayout;
        ImageView imgview;
        CardView cv;

        public userViewHolder(View itemView) {
            super(itemView);
            textnama = (TextView) itemView.findViewById(R.id.nama_user);
            textemail = (TextView) itemView.findViewById(R.id.email_user);
            circleImageView = (CircleImageView)itemView.findViewById(R.id.gambar_berita);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.pindah_adduser);
            imgview = (ImageView)itemView.findViewById(R.id.tambah_user);
            cv = (CardView)itemView.findViewById(R.id.user_list);


        }

    }
}