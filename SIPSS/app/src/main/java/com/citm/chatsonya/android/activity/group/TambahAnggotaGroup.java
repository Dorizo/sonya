package com.citm.chatsonya.android.activity.group;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.citm.chatsonya.android.Adapter.tambahanggotagroupAdapter;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.dto.UserResponse;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.http.AllDataService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahAnggotaGroup extends AppCompatActivity{

    Toolbar toolbar;
    AllDataService service;
    SharedPreferences mSettings;
    String key , confrence;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    tambahanggotagroupAdapter tambahanggotagroupAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambahanggotagroup);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView)findViewById(R.id.tambahangggotagroupbaru);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        setSupportActionBar(toolbar);
        service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
        mSettings = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        key =  mSettings.getString("key","missing");
        final Bundle b = getIntent().getExtras();
        confrence = b.getString("id_gruopconvernce");
        if (toolbar != null) {
            ((AppCompatActivity)this).setSupportActionBar(toolbar);
        }
        if (this instanceof AppCompatActivity) {
            ((AppCompatActivity)this).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        kirim();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void kirim(){
        Call<UserResponse> call = service.anggotaBaruadd("Bearer "+ key, confrence);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                Log.e("data ada", "onResponse: "+response.body().getResult());
                tambahanggotagroupAdapter = new tambahanggotagroupAdapter(response.body().getResult(), getBaseContext() , service ,key ,confrence);
                recyclerView.setAdapter(tambahanggotagroupAdapter);

            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

            }
        });

    }


}
