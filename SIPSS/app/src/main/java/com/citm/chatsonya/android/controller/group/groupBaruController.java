package com.citm.chatsonya.android.controller.group;

import com.citm.chatsonya.android.dto.model.Post;
import com.shipdream.lib.android.mvc.FragmentController;
import com.shipdream.lib.android.mvc.UiView;

public class groupBaruController extends FragmentController<Post, UiView> {
    @Override
    public Class<Post> modelType() {
        return Post.class;
    }
}
