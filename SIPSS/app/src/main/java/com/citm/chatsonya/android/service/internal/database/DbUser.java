package com.citm.chatsonya.android.service.internal.database;

import com.orm.SugarRecord;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class DbUser  extends SugarRecord {
    String id_online;
    String id_offline;
    String username;
    String firstname;
    String lastname;
    String biografi;
    String alamat;
    String photo_thumb_url;
    String phone;
    String access;
    String email;
    String token;
    String password;
    String photo;

    public DbUser() {

    }

}

