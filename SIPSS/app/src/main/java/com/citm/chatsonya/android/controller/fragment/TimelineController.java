package com.citm.chatsonya.android.controller.fragment;

import android.util.Log;

import com.citm.chatsonya.android.dto.model.ViewuitimelineModel;
import com.citm.chatsonya.android.dto.model.timelinemodelcontroller;
import com.citm.chatsonya.android.dto.timelineResponse;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.http.AllDataService;
import com.google.gson.Gson;
import com.shipdream.lib.android.mvc.FragmentController;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimelineController extends FragmentController<timelinemodelcontroller,ViewuitimelineModel> {

    AllDataService dataService;
    String TAG="timeline";
    @Override
    public Class<timelinemodelcontroller> modelType() {
        return timelinemodelcontroller.class;
    }

    public void viewtimeline(String key , String serach){
        dataService = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
        Call<timelineResponse> call =  dataService.gettimeline("Bearer "+key , "1");
        call.enqueue(new Callback<timelineResponse>() {
            @Override
            public void onResponse(Call<timelineResponse> call, Response<timelineResponse> response) {
                Gson gson = new Gson();
                Log.e(TAG, "onResponse: "+ call.request().url() );
                if(response.isSuccessful()){
                    Log.e(TAG, "onResponse: "+gson.toJson(response.body().getProfilResponse()));
                    getModel().setTimelinemodels(response.body().getCitm());
                    getModel().setUsersmodels(response.body().getProfilResponse());
                }else{
                    Log.e(TAG, "eror onResponse: "+gson.toJson(response.errorBody()) );

                }
                view.update();
            }

            @Override
            public void onFailure(Call<timelineResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );
            }
        });
    }


}
