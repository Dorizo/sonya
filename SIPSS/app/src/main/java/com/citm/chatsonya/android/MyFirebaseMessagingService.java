package com.citm.chatsonya.android;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.citm.chatsonya.android.activity.privatechat.privateChat;
import com.citm.chatsonya.android.dto.model.Message;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.view.group.groupbox;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SharedPreferences mSattings;

    DatabaseHandler dbHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        dbHandler = new DatabaseHandler(this);
        mSattings = getSharedPreferences("Settings", Context.MODE_PRIVATE);

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());


        }

        try{
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat crTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = crTime.format(c);
        Log.e(TAG, "onMessageReceived: "+remoteMessage.getData().toString() );
        if(remoteMessage.getData().get("group").equals("401")) {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(remoteMessage.getData().get("id_conversation"));
        } else if(remoteMessage.getData().get("group").equals("1")){
            if(mSattings.getString("id_user" , null).equals(remoteMessage.getData().get("sender_id"))){
                Log.e(TAG, "tidak akan di tampilkan karena user anda sama" );
            }else{
            Message m = new Message("1",remoteMessage.getData().get("reciver_id"),remoteMessage.getData().get("sender_id"), remoteMessage.getData().get("body"), remoteMessage.getData().get("id_conversation"), formattedDate,remoteMessage.getData().get("title"),remoteMessage.getData().get("kode_fungsi"));
            dbHandler.AddMessage(m);
            Log.e(TAG, "onMessageReceived: "+"coba group" );
            GROUPNOTIVICATION(remoteMessage.getData().get("body"), remoteMessage.getData().get("title"), remoteMessage.getData().get("sender_id"), remoteMessage.getData().get("reciver_id") , remoteMessage.getData().get("id_conversation"));

            }
            Intent pushgroup = new Intent(RetrofitInstance.PUSH_GROUP_NOTIFICATION);
            pushgroup.putExtra("message", remoteMessage.getData().get("body"));
            pushgroup.putExtra("reciver", remoteMessage.getData().get("reciver_id"));
            pushgroup.putExtra("toolbartitle", remoteMessage.getData().get("title"));
            pushgroup.putExtra("sender", remoteMessage.getData().get("sender_id"));
            pushgroup.putExtra("id_conversation", remoteMessage.getData().get("id_conversation"));
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushgroup);
            FirebaseMessaging.getInstance().subscribeToTopic(remoteMessage.getData().get("id_conversation"));


        }else {
            Message m = new Message("-1",remoteMessage.getData().get("reciver_id"),remoteMessage.getData().get("sender_id"), remoteMessage.getData().get("body"), remoteMessage.getData().get("id_conversation"), formattedDate,remoteMessage.getData().get("title"),remoteMessage.getData().get("kode_fungsi"));
            dbHandler.AddMessage(m);
            if (remoteMessage.getData().size() > 0) {
                Intent pushNotification = new Intent(RetrofitInstance.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", remoteMessage.getData().get("body"));
                pushNotification.putExtra("reciver", remoteMessage.getData().get("reciver_id"));
                pushNotification.putExtra("sender", remoteMessage.getData().get("sender_id"));
                pushNotification.putExtra("id_conversation", remoteMessage.getData().get("id_conversation"));
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                Log.e(TAG, "onMessageReceived: "+"coba biasa" );
                sendNotification(remoteMessage.getData().get("body"), remoteMessage.getData().get("title"), remoteMessage.getData().get("sender_id"), remoteMessage.getData().get("reciver_id"),remoteMessage.getData().get("id_conversation"));
            }
        }


        }catch (Exception e){
            Log.e(TAG, "onMessageReceived: "+e );
        }



    }


    // [END receive_message]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
//    private void scheduleJob() {
//        // [START dispatch_job]
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(MyJobService.class)
//                .setTag("my-job-tag")
//                .build();
//        dispatcher.schedule(myJob);
//        // [END dispatch_job]
//    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */

    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }


    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody , String title , String sender , String Reciver , String id_percakapan) {
        String gri = "-1";
        Intent intent = new Intent(this, privateChat.class);
        intent.putExtra("isGroup", gri.equals("-1") ? "0" : "1");
        intent.putExtra("group_id", gri);
        intent.putExtra("username",title);
        intent.putExtra("id" , sender);
        intent.putExtra("id_gruopconvernce" , id_percakapan);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        int notifyID = 1;
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }


        String channelId = title;
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_logos)
                        .setGroup(id_percakapan)
                        .setGroupSummary(true)
                        .setContentTitle(title)
                        .setColor(ContextCompat.getColor(this,R.color.colorPrimary))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        Random random = new Random();
        notificationManager.notify(random.nextInt() /* ID of notification */, notificationBuilder.build());
    }

    private void GROUPNOTIVICATION(String messageBody , String title , String sender , String Reciver , String id_percakapan) {
        String gri = "-1";
        Intent intent = new Intent(this, groupbox.class);
        intent.putExtra("isGroup", gri.equals("-1") ? "0" : "1");
        intent.putExtra("group_id", gri);
        intent.putExtra("username",title);
        intent.putExtra("toolbartitle",title);
        intent.putExtra("id" , sender);
        intent.putExtra("id_gruopconvernce" , id_percakapan);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        String channelId = title;
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_logos)
                        .setGroup(id_percakapan)
                        .setGroupSummary(true)
                        .setContentTitle(title)
                        .setColor(ContextCompat.getColor(this,R.color.colorPrimary))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        Random random = new Random();
        notificationManager.notify(random.nextInt() /* ID of notification */, notificationBuilder.build());
    }
}