package com.citm.chatsonya.android.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.chartActifity.CircleTransform;
import com.citm.chatsonya.android.dto.model.Message;
import com.citm.chatsonya.android.dto.model.timelinemodel;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

import static io.fabric.sdk.android.Fabric.TAG;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    Context mContext;
    ArrayList<Message> messagesList;
    String SELF, fontSize, today;
    Boolean showBubble, showStamp;
    ArrayList<timelinemodel> timeline;
    Gson gson = new Gson();


    public ChatAdapter(ArrayList<timelinemodel> timeline1) {
        timeline = timeline1;
        notifyDataSetChanged();
        Log.e("ddd", "ChatAdapter: "+timeline);
    }

    public ChatAdapter(Context context, ArrayList<Message> messagesList, String self, boolean showBubble, boolean showStamp, String fontSize,ArrayList<timelinemodel> timeline) {
        this.mContext = context;
        this.messagesList = messagesList;
        this.SELF = self;
        this.showBubble = showBubble;
        this.showStamp = showStamp;
        this.fontSize = fontSize;
        Calendar calendar = Calendar.getInstance();
        this.today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        this.timeline = timeline;
        Log.e("ddd", "ChatAdapter: "+timeline);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == 5) {
//            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatmodel_right, parent, false);
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatmodel_status_right, parent, false);
        }else if(viewType == 2){
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatmodel_status_right, parent, false);
        }else if(viewType == 4){
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatmodel_status_left, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatmodel_status_left, parent, false);
//            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatmodel_left, parent, false);
        }

        return new ViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        Message msg = messagesList.get(position);
        if (msg.getSender().equals(SELF)) {
            if(msg.getKode_fungsi().equals("status_private")){
             return 2;
            }else {
                return 5;
            }
        }else{
            if(msg.getKode_fungsi().equals("status_private")){
                return 4;
            }else {
                return 0;
            }
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try{
            String g = gson.toJson(timeline.get(0));
            if (timeline.get(0).getImage_timeline().equals("default.jpg")){
                holder.fotostatusimage.setVisibility(View.GONE);
                holder.statustext.setText(timeline.get(0).getText_timeline());
            }else{
                holder.statustext.setText(timeline.get(0).getText_timeline());
                Picasso.with(holder.itemView.getContext()).load(RetrofitInstance.Image_url+"uploads/"+timeline.get(0).getImage_timeline()).transform(new CircleTransform()).into(holder.fotostatusimage);


            }
            Log.e(TAG, "onBindViewHolder: "+g);
        }catch (Exception e){

        }
        if(messagesList.get(position).getKode_fungsi().toString().equals("status_private")){
            holder.linearLayout.setVisibility(View.VISIBLE);

//            Log.e("Logs", "datatimeline: "+timeline.get(0).getText_timeline() );
        }else{
            holder.linearLayout.setVisibility(View.GONE);
        }

        try {
//            holder.message.setText(timeline.get(0).getId());
            Log.e("Logs", "datatimeline: "+timeline.toString() );
            holder.message.setText(messagesList.get(position).getMessage());
        }catch (Exception e){
            holder.message.setText(messagesList.get(position).getMessage());
        }
    }



    public String getTimeStamp(String stamp, boolean isReceived) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (isReceived) { format.setTimeZone(TimeZone.getTimeZone("UTC")); }
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(stamp);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            timestamp = format.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    @Override
    public int getItemCount() {
        return messagesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView message, timestamp, sender , statustext;
        public CircleImageView icon;
        public ImageView status, imageView ,fotostatusimage;
        public LinearLayout linearLayout;


        public ViewHolder(View itemView) {
            super(itemView);
            message = (TextView) itemView.findViewById(R.id.txtMessage);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
            icon = (CircleImageView) itemView.findViewById(R.id.icon);
            sender = (TextView) itemView.findViewById(R.id.sender);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.satu);
            statustext = (TextView)itemView.findViewById(R.id.fotostatusmessage);
            fotostatusimage = (ImageView)itemView.findViewById(R.id.fotostatusimages);
        }
    }
}
