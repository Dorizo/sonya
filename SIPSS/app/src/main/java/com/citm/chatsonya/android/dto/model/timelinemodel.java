package com.citm.chatsonya.android.dto.model;

import android.net.Uri;

public class timelinemodel {
    String text_timeline,Users_id,image_timeline,color_text,created_at,id,name;
    Uri image;


    public String getText_timeline() {
        return text_timeline;
    }

    public void setText_timeline(String text_timeline) {
        this.text_timeline = text_timeline;
    }

    public String getUsers_id() {
        return Users_id;
    }

    public void setUsers_id(String users_id) {
        Users_id = users_id;
    }

    public String getImage_timeline() {
        return image_timeline;
    }

    public void setImage_timeline(String image_timeline) {
        this.image_timeline = image_timeline;
    }

    public String getColor_text() {
        return color_text;
    }

    public void setColor_text(String color_text) {
        this.color_text = color_text;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Uri getImage() {
        return image;
    }

    public void setImage(Uri image) {
        this.image = image;
    }
}
