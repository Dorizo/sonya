package com.citm.chatsonya.android.helpers.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.citm.chatsonya.android.dto.model.Inbox;
import com.citm.chatsonya.android.dto.model.Message;
import com.citm.chatsonya.android.dto.model.groupMembersModel;
import com.citm.chatsonya.android.dto.model.usersmodel;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.content.ContentValues.TAG;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DBVersion = 1;
    private static final String DBName = "weekiDatabase";

    public DatabaseHandler(Context context)
    {
        super(context, DBName, null, DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Table Query.
        Log.d("SQLite", "Creating tables ...");
        String usersTable = "CREATE TABLE IF NOT EXISTS users (id int, username VARCHAR(255), email VARCHAR(255), name VARCHAR(50), icon TEXT, status VARCHAR(130), creation DATETIME);";
        String messagesTable = "CREATE TABLE IF NOT EXISTS messages (message_id INTEGER PRIMARY KEY, group_id INTEGER DEFAULT -1, sender_id VARCHAR(255), receiver_id VARCHAR(255), message_type INTEGER, message TEXT, creation DATETIME, isSeen INTEGER, isError INTEGER, isReceived INTEGER,id_conversation TEXT, message_name TEXT, kode_fungsi TEXT);";
        String groupMembersTable = "CREATE TABLE IF NOT EXISTS groupMembers (id_conversation INTEGER, id int);";
        String blockTable = "CREATE TABLE IF NOT EXISTS blockedUsers (username VARCHAR(255), blockDate DATETIME);";
        String muteTable = "CREATE TABLE IF NOT EXISTS mutedUsers (username VARCHAR(255), group_id INTEGER);";
        db.execSQL(usersTable);
        db.execSQL(messagesTable);
        db.execSQL(groupMembersTable);
        db.execSQL(blockTable);
        db.execSQL(muteTable);
        Log.d("SQLite", "Query executed.");
    }

    public void resetDatabase() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM users");
        db.execSQL("DELETE FROM messages");
//        db.execSQL("DELETE FROM groups");
        db.execSQL("DELETE FROM groupMembers");
        db.execSQL("DELETE FROM blockedUsers");
        db.execSQL("DELETE FROM mutedUsers");
        db.close();
    }
    public void deleteuser(){

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS users");
        db.execSQL("DROP TABLE IF EXISTS messages");
        db.execSQL("DROP TABLE IF EXISTS groupMembers");
        db.close();


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS users");
        db.execSQL("DROP TABLE IF EXISTS messages");
//        db.execSQL("DROP TABLE IF EXISTS groups");
        db.execSQL("DROP TABLE IF EXISTS groupMembersTable");
        db.execSQL("DROP TABLE IF EXISTS blockedUsers");
        db.execSQL("DROP TABLE IF EXISTS mutedUsers");
        onCreate(db);
    }

    // Adding a new message on database.
    public long AddMessage(Message message)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("receiver_id", message.getReciver());
        values.put("sender_id", message.getSender());
        values.put("group_id", message.getGroupID());
        values.put("message_type", message.getMessageType());
        values.put("message", message.getMessage());
        values.put("creation", message.getCreation());
        values.put("isSeen", message.getStatus());
        values.put("isError", message.getError());
        values.put("isReceived", message.isReceived());
        values.put("id_conversation" , message.getId_conversation());
        values.put("message_name" , message.getMessage_name());
        values.put("kode_fungsi" , message.getKode_fungsi());
//        Log.e("ada ngak", "AddMessage: "+values );
        return db.insert("messages", null, values);
    }

    public long AddgroupMembers(groupMembersModel message){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id_conversation" , message.getId_conversation());
        values.put("id" , message.getId());
//        Log.e(TAG, "AddgroupMembers: "+values);
        return db.insert("groupMembers" , null , values);
    }



    public ArrayList<Message> getprivatechat(String id_user , String myself , String id_percakapan){

        ArrayList<Message> messagesArray = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM messages  where `sender_id` in ($sender,$reciver) and `receiver_id` in ($sender,$reciver) and group_id = '-1' and id_conversation = '$percakapan' order by creation ASC ".replace("$sender", id_user).replace("$reciver", myself).replace("$percakapan", id_percakapan), null);
        if (cursor.moveToFirst()) {

//            Log.e(TAG, "getprivatechat: "+cursor.getColumnName(0)+cursor.getString(0)+cursor.getColumnName(1)+cursor.getString(1)+cursor.getColumnName(2)+cursor.getString(2)+cursor.getColumnName(3)+cursor.getString(3)+cursor.getColumnName(4)+cursor.getString(4)+cursor.getColumnName(5)+cursor.getString(5)+cursor.getColumnName(6)+cursor.getString(6)+cursor.getColumnName(10)+cursor.getString(10));
            do {
//                int count = cursor.getColumnCount();
//                for (int i = 1; i <= count; i++)
//                {
//                    Log.e(TAG, "getprivatechat: "+cursor.getColumnName(i));
//                }
//                Log.e(TAG, "getprivatechat: "+cursor.getString(4)+" sender id"+cursor.getString(3) +" kode_fungsi "+cursor.getString(12));
                Message m = new Message("-1",cursor.getString(2), cursor.getString(3), cursor.getString(5),cursor.getString(10),cursor.getString(6),cursor.getString(11),cursor.getString(12));
                m.setMessageType(cursor.getInt(4));
                m.isReceived(cursor.getInt(8));
                m.setMessageID(cursor.getString(0));
                m.setStatus(cursor.getInt(6));
                m.setError(cursor.getInt(7));
                m.setId_conversation(cursor.getString(10));
                messagesArray.add(m);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return messagesArray;

    }

    public ArrayList<Message> Groupmessage(String conframs){

        ArrayList<Message> messagesArray = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM messages  where `id_conversation` = '$reciver'  order by creation ASC ".replace("$reciver", conframs), null);
        if (cursor.moveToFirst()) {
//            Log.e(TAG, "confrance: "+cursor.getColumnName(0)+cursor.getString(0)+cursor.getColumnName(1)+cursor.getString(1)+cursor.getColumnName(2)+cursor.getString(2)+cursor.getColumnName(3)+cursor.getString(3)+cursor.getColumnName(4)+cursor.getString(4)+cursor.getColumnName(5)+cursor.getString(5)+cursor.getColumnName(6)+cursor.getString(6)+cursor.getColumnName(10)+cursor.getString(10));
            do {
//                Log.e(TAG, "getprivatechat: "+cursor.getString(4)+" sender id"+cursor.getString(3) );
                Message m = new Message(cursor.getString(1),cursor.getString(2), cursor.getString(3), cursor.getString(5),cursor.getString(10),cursor.getString(6),cursor.getString(11),cursor.getString(12));
                m.setMessageType(cursor.getInt(4));
                m.isReceived(cursor.getInt(8));
                m.setMessageID(cursor.getString(0));
                m.setStatus(cursor.getInt(6));
                m.setError(cursor.getInt(7));
                m.setId_conversation(cursor.getString(10));
                messagesArray.add(m);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return messagesArray;

    }



    // Retrieving all messages from database accordingly.
    public ArrayList<Inbox> getAllMessages()
    {
        ArrayList<Inbox> inboxArray = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from messages GROUP BY id_conversation ORDER BY creation DESC", null);
        if (cursor.moveToFirst()) {
//            Log.e(TAG, "getprivatechat: "+cursor.getColumnName(0)+cursor.getString(0)+cursor.getColumnName(1)+cursor.getString(1)+cursor.getColumnName(2)+cursor.getString(2)+cursor.getColumnName(3)+cursor.getString(3)+cursor.getColumnName(4)+cursor.getString(4)+cursor.getColumnName(5)+cursor.getString(5)+cursor.getColumnName(6)+cursor.getString(6)+cursor.getColumnName(10)+cursor.getString(10));
            do {
                    Inbox inbox = new Inbox(cursor.getString(0),cursor.getString(1), cursor.getString(2), cursor.getString(3),cursor.getString(5),cursor.getString(10),cursor.getString(11));
                    inboxArray.add(inbox);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return inboxArray;
    }



    public void MarkMessagesAsRead(String group_id, String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (group_id != "-1") {
            String query = ("UPDATE messages SET isSeen = 1 WHERE group_id = '$id'".replace("$id", group_id));
            db.execSQL(query);
        }
        else {
            String query = ("UPDATE messages SET isSeen = 1 WHERE group_id = -1 AND receiver_id = '$username'".replace("$username", username));
            db.execSQL(query);
        }
        db.close();
    }

    public boolean DeleteAllMessages() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("messages", null, null);
        db.close();
        return true;
    }
    public void RemoveGroupMember(String group_id, String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("groupMembers", "group_id = '$id' AND username = '$user'".replace("$id", group_id).replace("$user", username), null);
    }

    public boolean isGroupMember(String group_id, String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM groupMembers WHERE group_id = '$id' AND username = '$user'".replace("$id", group_id).replace("$user", username), null);
        int i = cursor.getCount();
        cursor.close();
        return i >= 1;
    }
    public ArrayList<usersmodel> getGroupMember(String id_group){
            ArrayList<usersmodel> usersArray = new ArrayList<>();
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT users.* , groupMembers.id_conversation as id_pecakapan FROM groupMembers Join users On groupMembers.id = users.id", null);
            if (cursor.moveToFirst()) {
                do {
                    usersmodel u = new usersmodel(cursor.getString(0) , cursor.getString(1) , cursor.getString(2) , cursor.getString(3) , cursor.getString(4) , cursor.getString(5), cursor.getString(6));
                    usersArray.add(u);
                } while (cursor.moveToNext());
            }
            cursor.close();
            return usersArray;
    }


    public ArrayList<usersmodel> getAllUsers()
    {
        ArrayList<usersmodel> usersArray = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM users ORDER BY name", null);
        if (cursor.moveToFirst()) {
            do {
                usersmodel u = new usersmodel(cursor.getString(0) , cursor.getString(1) , cursor.getString(2) , cursor.getString(3) , cursor.getString(4) , cursor.getString(5), cursor.getString(6));
                usersArray.add(u);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return usersArray;
    }

    public boolean AddUser(usersmodel user)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (isUserExists(user.getName())) {
            values.put("id" , user.getId());
            values.put("email", user.getEmail());
            values.put("name", user.getName());
            values.put("icon", user.getImage());
            values.put("status", user.getEmail());
            values.put("creation", user.getCreated_at());
            db.update("users", values, ("username = '$user'").replace("$user", user.getName()), null);
        }
        else {
            values.put("username", user.getName());
            values.put("id" , user.getId());
            values.put("email", user.getEmail());
            values.put("name", user.getName());
            values.put("icon", user.getImage());
            values.put("status", user.getEmail());
            values.put("creation", user.getCreated_at());
            db.insert("users", null, values);
        }
        db.close();
        return true;
    }

    // Check if there's already a user in database or not.
    public boolean isUserExists(String username)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM users WHERE id = '" + username + "'", null);
        int userCount = cursor.getCount();
//        Log.e(TAG, "isUserExists: "+username);
        cursor.close();
        return userCount == 1;
    }

    public String getuser(String username)
    {
        String strings = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM users WHERE id = '" + username + "'", null);
        if (cursor.moveToFirst()) {
            strings = cursor.getString(1);
        }
//        Log.e(TAG, "isUserExists: "+username);
        cursor.close();
        return strings;
    }

    // Delete whole conversation with specific username.
    public int DeleteConversation(String group_id,String username)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            if (group_id.equals("-1")) {
                db.delete("messages", "id_conversation = '$sender' AND group_id = -1".replace("$sender", username), null);
            }else{
                db.delete("messages", "id_conversation = '$sender'".replace("$sender", username), null);
            }
        }catch (Exception e){
            db.delete("messages", "id_conversation IS NULL", null);
        }

        db.close();
        return 1;
    }

    public void deleteuser(String id_user){
      SQLiteDatabase db = this.getWritableDatabase();
      db.delete("users" , "id = '$iduser' ".replace("$iduser" , id_user) , null);
      db.close();
    }

    // Retrieve user information.
    public usersmodel GetUserInfo(String username)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM users WHERE id = '" + username + "'", null);
        usersmodel u;
        if(cursor.moveToFirst()) {
            u = new usersmodel(cursor.getString(0) , cursor.getString(1) , cursor.getString(2) , cursor.getString(3) , cursor.getString(4) , cursor.getString(5), cursor.getString(6));
            cursor.close();
            return u;
        }else {
            u = new usersmodel(username, username,  username);
        }
        cursor.close();
        return u;
    }

    // Block user using its username.
    public void BlockUser(String username)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("username", username);
        values.put("blockDate", DateFormat.getDateTimeInstance().format(new Date()));
        db.insert("blockedUsers", null, values);
        db.close();
    }

    // Check whether the username is currently blocked or not.
    public boolean isBlocked(String username)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM blockedUsers WHERE username = '" + username + "'", null);
        int i = cursor.getCount();
        cursor.close();
        return i == 1;
    }

    // Unblock user using its username.
    public int unBlock(String username)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("blockedUsers", "username = '$user'".replace("$user", username), null);
        db.close();
        return 1;
    }

    // Retrieve a list of blocked users.
    public ArrayList<blockedUsers> RetrieveBlockedUsers()
    {
        ArrayList<blockedUsers> users = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM blockedUsers", null);
        if (cursor.moveToFirst()) {
            do {
                blockedUsers bUsers = new blockedUsers(cursor.getString(0), cursor.getString(1));
                users.add(bUsers);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return users;
    }

    // Class for holding blocked users details.
    public class blockedUsers
    {
        String p_username;
        String p_when;

        public blockedUsers(String username, String when) {
            p_username = username;
            p_when = when;
        }

        public String getUsername() { return p_username; }
        public String getBlockDate() { return p_when; }
    }

    // Mute user using its username.
    public void MuteUser(String username, String group_id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (group_id.equals("-1")) {
            values.put("username", username);
        }
        else { values.put("group_id", Integer.parseInt(group_id)); }
        db.insert("mutedUsers", null, values);
        db.close();
    }

    // Unmute user using its username.
    public int UnmuteUser(String username, String group_id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        if (group_id.equals("-1")) {
            db.delete("mutedUsers", "username = '$id'".replace("$id", username), null);
            db.close();
        }
        else {
            db.delete("mutedUsers", "group_id = '$id'".replace("$id", group_id), null);
            db.close();
        }
        return 1;
    }

    // Check whether the username or group is currently muted or not.
    public boolean isMuted(String group_id, String username)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        int i;
        if (group_id.equals("-1")) {

            Cursor cursor = db.rawQuery("SELECT * FROM mutedUsers WHERE username = '$username'".replace("$username", username), null);
            i = cursor.getCount();
            cursor.close();
        }
        else
        {
            Cursor cursor = db.rawQuery("SELECT * FROM mutedUsers WHERE group_id = '$id'".replace("$id", group_id), null);
            i = cursor.getCount();
            cursor.close();
        }
        return i == 1;
    }
}
