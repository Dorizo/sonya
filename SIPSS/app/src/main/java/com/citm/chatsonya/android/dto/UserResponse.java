package com.citm.chatsonya.android.dto;

import com.citm.chatsonya.android.dto.model.Accessadmin;
import com.citm.chatsonya.android.dto.model.usersmodel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserResponse {
    @SerializedName("citm")
    private  ArrayList<usersmodel> result;

    @SerializedName("adminaccess")
    private Accessadmin accessadmins;

    public Accessadmin getAccessadmins() {
        return accessadmins;
    }

    public void setAccessadmins(Accessadmin accessadmins) {
        this.accessadmins = accessadmins;
    }

    public ArrayList<usersmodel> getResult() {
        return result;
    }

    public void setResult(ArrayList<usersmodel> result) {
        this.result = result;
    }
}
