package com.citm.chatsonya.android.activity.privatechat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citm.chatsonya.android.Adapter.ChatAdapter;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.UserResponse;
import com.citm.chatsonya.android.dto.model.Message;
import com.citm.chatsonya.android.dto.model.timelinemodel;
import com.citm.chatsonya.android.dto.model.usersmodel;
import com.citm.chatsonya.android.dto.timelineResponse;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.http.AllDataService;
import com.google.gson.Gson;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconMultiAutoCompleteTextView;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class privateChat extends AppCompatActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    private static final String TAG = "nulled" ;
    ImageButton btnEmoticons, btnImage ,btndocument;
        LinearLayout btnSend, btnCancel, chatForm;
        EmojiconMultiAutoCompleteTextView txtMessage;
        FrameLayout emojiconLayout;
        RecyclerView ChatView;
        ChatAdapter cAdapter;
        ArrayList<Message> messagesList;
        LinearLayoutManager layoutManager;
        DatabaseHandler dbHandler;
        SharedPreferences sharedPref ,mSettings;
         BroadcastReceiver mRegistrationBroadcastReceiver;
        ProgressDialog progressDialog;
        Toolbar toolbar;
        Bitmap bitmap;
        int isGroup;
        int isMuted;
        int isBlocked;
        boolean isEmojiKeyboardShown = false, isMember;
        String group_id = "-1";
        String p_name;
        String p_username;
        String SELF, bitmap_path , RECIVER ,USERNAME , id_conversation , get_id_conversation;
        Menu menu;
        SimpleDateFormat crTime;
        Calendar c;
        AllDataService service;
        String key , userself;
        usersmodel user;
        ArrayList<timelinemodel> r;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.privatechat_layout);
                toolbar = (Toolbar) findViewById(R.id.toolbar);
                setSupportActionBar(toolbar);
                if (toolbar != null) {
                    ((AppCompatActivity)this).setSupportActionBar(toolbar);
                }
                if (this instanceof AppCompatActivity) {
                    ((AppCompatActivity)this).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                }




                emojiconLayout= (FrameLayout) findViewById(R.id.emojicons);
                btnEmoticons = (ImageButton) findViewById(R.id.btnEmoticons);
                btnImage = (ImageButton) findViewById(R.id.btnImage);
                btnSend = (LinearLayout) findViewById(R.id.btnSend);
                btnCancel = (LinearLayout) findViewById(R.id.btnCancel);
                txtMessage = (EmojiconMultiAutoCompleteTextView) findViewById(R.id.txtMessage);
                btndocument = (ImageButton)findViewById(R.id.document);
                chatForm = (LinearLayout) findViewById(R.id.form);
                ChatView = (RecyclerView) findViewById(R.id.viewChat);
                layoutManager = new LinearLayoutManager(getBaseContext(),LinearLayoutManager.VERTICAL, false);
                ChatView.setLayoutManager(layoutManager);
                dbHandler = new DatabaseHandler(this);
                final Gson gson = new Gson();
                sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                mSettings = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                key =  mSettings.getString("key","missing");
                userself =  mSettings.getString("user","missing");
                service= RetrofitInstance.RetrofitInstance().create(AllDataService.class);
                 Bundle b = getIntent().getExtras();
                ChatView.setHasFixedSize(true);


            if(b!=null){
                        RECIVER = mSettings.getString("id_user" , null);
                        SELF = b.getString("id");
                        USERNAME = b.getString("username");
                        get_id_conversation = b.getString("id_gruopconvernce");
                    }else{
                        SELF = null;
                        RECIVER = null;
                        USERNAME = null;
                    }
                    toolbar.setTitle(USERNAME);

                c = Calendar.getInstance();
                crTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                messagesList = new ArrayList<>();


            int[] array ={ Integer.parseInt(RECIVER),Integer.parseInt(SELF)};
            Arrays.sort(array);
            final String nh = String.valueOf(array[0])+String.valueOf(array[1]);
            id_conversation = nh;
//            Log.e(TAG, "onCreate:id_conversation "+id_conversation );

                try{
                messagesList = dbHandler.getprivatechat(SELF , RECIVER , get_id_conversation);
                //membuat kode unik untuk id_CONVERSASION dengan rumus id user sendiri dan user penerima
                    
                }catch (Exception e){
                    Log.e(TAG, "onCreate: "+e );
                }


                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        Call<timelineResponse> call = service.gettimelinesingle("Bearer "+ key, get_id_conversation);
                        call.enqueue(new Callback<timelineResponse>() {
                            @Override
                            public void onResponse(Call<timelineResponse> call, Response<timelineResponse> response) {


//                                Log.e(TAG, "onResponse: sedddddddddddddd"+response.body().getCitm() );
                                if(response.isSuccessful()){

                                    r = response.body().getCitm();
//                                    cAdapter= new ChatAdapter(r);
                                    cAdapter = new ChatAdapter(getApplicationContext(),messagesList, SELF, sharedPref.getBoolean("chat_showBubble", true), sharedPref.getBoolean("chat_showTimestamps", false), sharedPref.getString("chat_fontSize", "14"), r);

                                    cAdapter.notifyDataSetChanged();
                                    ChatView.setAdapter(cAdapter);

                                }
                            }

                            @Override
                            public void onFailure(Call<timelineResponse> call, Throwable t) {

                            }
                        });

                    }
                }).start();
                //status terakhir disini





//            Log.e("sodakoh", "onBindViewHolder: "+ gson.toJson(r));

                //end status terakhir
                cAdapter = new ChatAdapter(this,messagesList, SELF, sharedPref.getBoolean("chat_showBubble", true), sharedPref.getBoolean("chat_showTimestamps", false), sharedPref.getString("chat_fontSize", "14"), r);
                ChatView.scrollToPosition(messagesList.size()-1);
                ChatView.setItemAnimator(new DefaultItemAnimator());
                ChatView.setAdapter(cAdapter);
                Intent intent = getIntent();
                isGroup = Integer.parseInt(b.getString("isGroup"));
                if (isGroup == 1) {
                    Log.e(TAG, "disini grub");
                }
                else
                {
                loadUserInfo(intent.getStringExtra("id"));
                }

                dbHandler.MarkMessagesAsRead(group_id, ""+p_username);
                setResult(Activity.RESULT_OK, new Intent());


                btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        String strCreation = crTime.format(c.getTime());
                        if (txtMessage.getText().toString().trim().isEmpty()) {
                        return;
                        }
                        //jika 0 private chat
                           if (isGroup == 0) {
                            Message msg;
                            if (bitmap != null) {
                            msg = new Message("-1", SELF, RECIVER, bitmap_path, id_conversation, strCreation , USERNAME ,"private");
                            msg.setMessageType(1);
                            msg.setStatus(1);
                            } else {
                            msg = new Message("-1", SELF, RECIVER, txtMessage.getText().toString(), id_conversation, strCreation ,USERNAME ,"private");
                            msg.setMessageType(0);
                            msg.setStatus(1);
                            }
                            long id = dbHandler.AddMessage(msg);
                            if (id != -1) {
                            Call<PostResponse> call = service.pesan("Bearer "+ key, txtMessage.getText().toString(), userself , SELF, RECIVER , id_conversation,"-1" ,"private");
                            call.enqueue(new Callback<PostResponse>() {
                                @Override
                                public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {

                                    if (response.isSuccessful()){
                                        Log.e(TAG, "onResponse: "+response.body().toString() );
                                    }else{
                                        Log.e(TAG, "onResponse: "+response.errorBody().source() );
                                    }

                                }

                                @Override
                                public void onFailure(Call<PostResponse> call, Throwable t) {
                                    Log.e(TAG, "onFailure: "+t );

                                }
                            });

                            if (bitmap != null) {
                                    messagesList.add(msg);
                                    resetMessage();
                                    cAdapter.notifyDataSetChanged();
                            } else {
                                    messagesList.add(msg);
                                    resetMessage();
                                    cAdapter.notifyDataSetChanged();
                            }


                            } else {
                            finish();
                            }
                            }


                dbHandler.MarkMessagesAsRead(group_id, "" + p_username);
                scrollToLast();
                }
                });

                btndocument.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "onClick: dokument button");

                    }
                });

                    btnEmoticons.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                                isEmojiKeyboardShown = !isEmojiKeyboardShown;
                                showEmojiconFragment(isEmojiKeyboardShown);
                                if (isEmojiKeyboardShown) {
                                btnEmoticons.setImageResource(R.drawable.ic_keyboard);
                                txtMessage.requestFocus();
                                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(txtMessage.getWindowToken(), 0);
                                } else {
                                btnEmoticons.setImageResource(R.drawable.emoticons_button);
                                txtMessage.requestFocus();
                                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.showSoftInput(txtMessage, InputMethodManager.SHOW_IMPLICIT);
                                }
                                }
                    });

                txtMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                                showEmojiconFragment(false);
                                btnEmoticons.setImageResource(R.drawable.emoticons_button);
                            }
                });

                txtMessage.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (txtMessage.getText().toString().trim().isEmpty()) {
                            btnSend.setVisibility(View.GONE);
                            } else {
                            btnSend.setVisibility(View.VISIBLE);
                            }
                            }

                    @Override
                    public void afterTextChanged(Editable s) {
                            }
                });

                txtMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (sharedPref.getBoolean("chat_enterSend", true)) {
                            if (actionId == EditorInfo.IME_ACTION_DONE) {
                            btnSend.performClick();
                            return true;
                            }
                            }
                            return false;
                            }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                resetMessage();
                }
                });

                btnImage.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                showFileChooser();
                }
                });


                mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                      if (intent.getAction().equals(RetrofitInstance.PUSH_NOTIFICATION)) {
                            // new push notification is received

                          String strCreation = crTime.format(c.getTime());
                          Message msg;
                          String message = intent.getStringExtra("message");
                          String reciver = intent.getStringExtra("sender");
                          String sender = intent.getStringExtra("reciver");
                          String id_conversationpart = intent.getStringExtra("id_conversation");
//                          Log.e(TAG, "onReceive: "+id_conversation );
                          if (bitmap != null) {
                              msg = new Message("-1", sender, reciver, bitmap_path, id_conversationpart, strCreation , USERNAME ,"private");
                              msg.setMessageType(1);
                              msg.setStatus(1);
                              msg.setId_conversation(id_conversation);
                          } else {
                              msg = new Message("-1", sender, reciver, message, id_conversationpart, strCreation , USERNAME ,"private");
                              msg.setMessageType(0);
                              msg.setStatus(1);
                              msg.setId_conversation(id_conversation);
                          }
//
                          Log.e(TAG, "onReceive: "+id_conversationpart+"sama ngak "+id_conversation );

                          if (id_conversationpart.equals(id_conversation)){
                              messagesList.add(msg);
                              cAdapter.notifyDataSetChanged();
                              scrollToLast();
                          }

                        }
                    }
                };




                }

        private void resetMessage() {
                txtMessage.setText(null);
                txtMessage.setEnabled(true);
                btnCancel.setVisibility(View.GONE);
                btnEmoticons.setVisibility(View.VISIBLE);
                btnSend.setVisibility(View.GONE);
                bitmap = null;
        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
                    super.onActivityResult(requestCode, resultCode, data);
                    if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
                    Uri filePath = data.getData();
                    try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                    bitmap_path = filePath.toString();
                    txtMessage.setText("Press 'Send' to send this image.");
                    txtMessage.setEnabled(false);
                    btnCancel.setVisibility(View.VISIBLE);
                    btnEmoticons.setVisibility(View.GONE);
                    btnSend.setVisibility(View.VISIBLE);
                    }
                    catch (IOException ex) {
                    Log.d("Chat", "" + ex.getMessage());
                    }
                    }
                    else if (requestCode == 2 && resultCode == RESULT_OK) {
                    ArrayList<String> arrayUsers = data.getStringArrayListExtra("selectedUsers");
                    ArrayList<usersmodel> members = new ArrayList<>();
                    for (String i : arrayUsers) {
                    usersmodel u = dbHandler.GetUserInfo(i);
                    members.add(u);
                    }
                    addMembers(members);
                    }
            }

        @Override
        protected void onResume() {
                super.onResume();

                LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(RetrofitInstance.PUSH_NOTIFICATION));
                }

        @Override
        protected void onPause() {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
                super.onPause();
                }
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
                return true;
                }

        void loadUserInfo(final String username)
        {
                user = dbHandler.GetUserInfo(username);
                if (!dbHandler.isUserExists(user.getId()) && isBlocked != 1) {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("New user");
                builder.setMessage("This user is not in your friend list. Do you wanna add this person?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {



//                    dbHandler.AddUser(user);
                    useradd(user.getId());

                }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                }
                });

                AlertDialog alert = builder.create();
                alert.show();
                }
        }




        void useradd(String user_id){
            Call<UserResponse> call = service.getsinggleuser("Bearer "+ key,user_id);
            call.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                    if(response.isSuccessful()){
                        String iduser = response.body().getResult().get(0).getId();
                        String nama = response.body().getResult().get(0).getName();
                        String email = response.body().getResult().get(0).getEmail();
                        String telp = response.body().getResult().get(0).getTelp();
                        String image = response.body().getResult().get(0).getImage();
                        user = new usersmodel(iduser,nama,email,telp,image ,"","");
                        dbHandler.AddUser(user);
                    }else {
                        Log.e(TAG, "onResponse: "+response.errorBody().source() );
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {

                }
            });
        }

        void scrollToLast()
        {
        if (cAdapter.getItemCount() > 1) {
        ChatView.getLayoutManager().scrollToPosition(cAdapter.getItemCount() - 1);
        }
        }


        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
                int id = item.getItemId();
                switch (id){
                    case android.R.id.home:
                       super.onBackPressed();
                        return true;
                }
                return super.onOptionsItemSelected(item);

        }

        @Override
        public void onEmojiconClicked(Emojicon emojicon) {
                EmojiconsFragment.input(txtMessage, emojicon);
                }

        @Override
        public void onEmojiconBackspaceClicked(View v) {
                EmojiconsFragment.backspace(txtMessage);
                }

        private void showEmojiconFragment(boolean isVisible) {
                if (isVisible) {
                getSupportFragmentManager().beginTransaction().replace(R.id.emojicons, EmojiconsFragment.newInstance(false)).commit();
                emojiconLayout.setVisibility(View.VISIBLE);
                }
                else {
                emojiconLayout.setVisibility(View.GONE);
                }
                }

        public String getStringImage(Bitmap bmp){
                if (bmp == null)
                return "";

                ByteArrayOutputStream output = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, output);
                byte[] imageBytes = output.toByteArray();
                String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                return encodedImage;
                }

        private void showFileChooser() {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
                }

        private void addMembers(final ArrayList<usersmodel> membersList) {
                }

        private void updateGroupDescription(final String status) {
                }

}