

package com.citm.chatsonya.android.http.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoInspectionStaPrasaranaData implements Serializable {

    @SerializedName("id")
    public String id_online;
    public String id_local;
    public String inspection_sta_id;
    public String c_1_kiri_selected;
    public String c_1_kanan_selected;
    public String c_2_1_kiri_caption;
    public String c_2_1_kanan_caption;
    public String c_2_2_kiri_caption;
    public String c_2_2_kanan_caption;
    public String c_3_kiri_selected;
    public String c_3_kanan_selected;
    public String c_4_kiri_selected;
    public String c_4_kanan_selected;
    public String c_5_kiri_selected;
    public String c_5_kanan_selected;
    public String c_6_kiri_selected;
    public String c_6_kanan_selected;
    public String d_1_kiri_caption;
    public String d_1_kanan_caption;
    public String d_2_1_1_kiri_caption;
    public String d_2_1_1_kanan_caption;
    public String d_2_1_2_kiri_caption;
    public String d_2_1_2_kanan_caption;
    public String d_2_2_1_kiri_caption;
    public String d_2_2_1_kanan_caption;
    public String d_2_3_1_kiri_caption;
    public String d_2_3_1_kanan_caption;
    public String d_2_3_2_kiri_caption;
    public String d_2_3_2_kanan_caption;
    public String d_2_4_1_kiri_caption;
    public String d_2_4_1_kanan_caption;
    public String d_2_4_2_kiri_caption;
    public String d_2_4_2_kanan_caption;
    public String e_1_kiri_caption;
    public String e_1_kanan_caption;
    public String e_2_1_1_kiri_caption;
    public String e_2_1_1_kanan_caption;
    public String e_2_1_2_kiri_caption;
    public String e_2_1_2_kanan_caption;
    public String e_2_2_1_kiri_caption;
    public String e_2_2_1_kanan_caption;
    public String e_2_3_1_kiri_caption;
    public String e_2_3_1_kanan_caption;
    public String e_2_3_2_kiri_caption;
    public String e_2_3_2_kanan_caption;
    public String e_2_4_1_kiri_caption;
    public String e_2_4_1_kanan_caption;
    public String e_2_4_2_kiri_caption;
    public String e_2_4_2_kanan_caption;
    public String f_1_kiri_caption;
    public String f_1_kanan_caption;
    public String f_2_1_kiri_caption;
    public String f_2_1_kanan_caption;
    public String f_2_2_kiri_caption;
    public String f_2_2_kanan_caption;

    public String kode_kiri;
    public String kode_kanan;
    public String bangun_tahun_kiri;
    public String bangun_tahun_kanan;
    public String bangun_biaya_kiri;
    public String bangun_biaya_kanan;
    public String bangun_sumber_dana_kiri;
    public String bangun_sumber_dana_kanan;
    public String perbaikan_tahun_kiri;
    public String perbaikan_tahun_kanan;
    public String perbaikan_biaya_kiri;
    public String perbaikan_biaya_kanan;
    public String perbaikan_sumber_dana_kiri;
    public String perbaikan_sumber_dana_kanan;
    public String pelindung_crest_kiri;
    public String pelindung_crest_kanan;
    public String pelindung_dinding_kiri;
    public String pelindung_dinding_kanan;
    public String pelindung_crest_tebal_kiri;
    public String pelindung_crest_tebal_kanan;
    public String pelindung_dinding_tebal_kiri;
    public String pelindung_dinding_tebal_kanan;
    public String bahan_inti_kiri;
    public String bahan_inti_kanan;


    public String user_added;
    public String user_modified;
    public String date_added;
    public String date_modified;

    public DtoInspectionStaPrasaranaData() {

    }

}
