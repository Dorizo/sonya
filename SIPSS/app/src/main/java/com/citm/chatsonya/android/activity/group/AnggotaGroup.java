package com.citm.chatsonya.android.activity.group;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.citm.chatsonya.android.Adapter.AnggotaGroupAdapter;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.dto.UserResponse;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.http.AllDataService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnggotaGroup extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    Button tambahanggota;
    DatabaseHandler databaseHandler;
    AllDataService service;
    SharedPreferences mSettings;
    RecyclerView anggotalistgroup;
    AnggotaGroupAdapter anggotaGroupAdapter;
    LinearLayoutManager linearLayoutManager;
    Context context;
    String conf;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anggotagroup);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tambahanggota = (Button)findViewById(R.id.tambahanggota);
        tambahanggota.setOnClickListener(this);
        service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
        mSettings = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        anggotalistgroup = (RecyclerView)findViewById(R.id.anggotalistgroup);
        linearLayoutManager = new LinearLayoutManager(this);
        anggotalistgroup.setLayoutManager(linearLayoutManager);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            ((AppCompatActivity)this).setSupportActionBar(toolbar);
        }
        if (this instanceof AppCompatActivity) {
            ((AppCompatActivity)this).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        final Bundle b = getIntent().getExtras();
        conf = b.getString("id_gruopconvernce");
        Call<UserResponse> call = service.getusermembergroup("Bearer "+mSettings.getString("key","missing") ,b.getString("id_gruopconvernce"));
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful()){
                    Log.d("data ada", "onResponse: "+response.body().getResult().toString());
//                    anggotaGroupAdapter = new AnggotaGroupAdapter(response.body().getResult() ,context ,service ,mSettings.getString("key","missing"), b.getString("id_gruopconvernce"));
                    anggotaGroupAdapter = new AnggotaGroupAdapter(response.body().getResult() ,context ,service ,mSettings.getString("key","missing"), b.getString("id_gruopconvernce") , response.body().getAccessadmins());
                    anggotalistgroup.setAdapter(anggotaGroupAdapter);
                    try {
                        if(response.body().getAccessadmins().getStatus_acces().equals("1")){
                            tambahanggota.setVisibility(View.VISIBLE);

                        }else {
                            tambahanggota.setVisibility(View.GONE);
                        }
                    }catch (Exception E){

                    }


                }else {
                    Log.e("error respon", "onResponse: "+response.errorBody().toString() );
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onClick(View view) {
    switch (view.getId()){
        case R.id.tambahanggota:
            Intent intent = new Intent(getBaseContext(),TambahAnggotaGroup.class);
            intent.putExtra("id_gruopconvernce", conf);
            startActivity(intent);
            break;

    }
    }


}