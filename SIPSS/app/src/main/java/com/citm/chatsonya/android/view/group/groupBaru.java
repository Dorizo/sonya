package com.citm.chatsonya.android.view.group;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.citm.chatsonya.android.Adapter.groupBaruAdapter;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.group.groupBaruController;
import com.citm.chatsonya.android.controller.splashcontroller;
import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.model.Message;
import com.citm.chatsonya.android.dto.model.groupMembersModel;
import com.citm.chatsonya.android.dto.model.usersmodel;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.http.AllDataService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.shipdream.lib.android.mvc.MvcFragment;
import com.shipdream.lib.android.mvc.NavigationManager;
import com.shipdream.lib.android.mvc.Reason;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static net.alhazmy13.mediapicker.Image.ImageTags.Tags.TAG;


public class groupBaru extends MvcFragment<groupBaruController>{
    Toolbar mToolbar;
    Context context;
    @Inject
    NavigationManager navigationManager;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    DatabaseHandler db;
    groupBaruAdapter groupbaruAdapter;
    List<usersmodel> mModelList;
    EditText judulgruop;
    AllDataService service;
    SharedPreferences msetting;
    @Override
    protected Class<groupBaruController> getControllerClass() {
        return groupBaruController.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_groupbaru;
    }


    @Override
    protected void onViewReady(View view, Bundle savedInstanceState, Reason reason) {
        super.onViewReady(view, savedInstanceState, reason);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (mToolbar != null) {
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        }
        if (getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setHasOptionsMenu(true);
        mToolbar.setTitle("Group Add User");
        recyclerView = (RecyclerView)view.findViewById(R.id.groupbaruchaklist);
        judulgruop = (EditText)view.findViewById(R.id.judulgroup);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getContext());
        db = new DatabaseHandler(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        service= RetrofitInstance.RetrofitInstance().create(AllDataService.class);
        msetting = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);




    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.groupbaru_x_menu , menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigationManager.navigate(this).back();
                break;
            case R.id.donegroupbaru:
                if(judulgruop.getText().length() != 0){
                    Random rand = new Random();
                    int n = rand.nextInt(1000000);
                    String id_confrance = "group"+n;
                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat crTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = crTime.format(c);
                    Message pesangruop = new Message("2" , "sender","reciver","Group Berhasil dibuat "+judulgruop.getText() ,id_confrance ,formattedDate ,  judulgruop.getText().toString(),"private");
                    db.AddMessage(pesangruop);
                    mModelList =  groupbaruAdapter.getadapterusermodel();

                    for (usersmodel m : mModelList){
                    navigationManager.navigate(context).to(splashcontroller.class);
                        groupMembersModel gmm = new groupMembersModel(id_confrance , m.getId());
                        db.AddgroupMembers(gmm);
                        kirim(msetting.getString("key" , "missing") ,"Group Berhasil dibuat "+judulgruop.getText().toString(),judulgruop.getText().toString(), m.getId(), msetting.getString("id_user" , null),id_confrance);
                     }
                     // create group disini dengan subcrabe untuk push notivikasi group
                    FirebaseMessaging.getInstance().subscribeToTopic(id_confrance);
                    Log.d(TAG, "onOptionsItemSelected: subscrabe "+id_confrance);
                }
                break;

        }

        return super.onOptionsItemSelected(item);

    }

    private void kirim(String key , String message , String userselfssss , String SELF , String RECIVER , String id_conversation){

        Call<PostResponse> call = service.pesan("Bearer "+ key, message, userselfssss , SELF, RECIVER , id_conversation, "1","private");
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {

                if (response.isSuccessful()){
                    Log.e(TAG, "onResponse: "+response.body().toString() );
                }else{
                    Log.e(TAG, "onResponse: "+response.errorBody().source() );
                }

            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );

            }
        });
    }

    @Override
    public void update() {
        Log.e(TAG, "update: grub baru" +db.getAllUsers());
        if (db.getAllUsers() !=null) {
            groupbaruAdapter = new groupBaruAdapter(db.getAllUsers(), navigationManager, this , db);
            recyclerView.setAdapter(groupbaruAdapter);
        }


    }


}
