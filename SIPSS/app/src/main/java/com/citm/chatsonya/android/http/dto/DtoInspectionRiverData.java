

package com.citm.chatsonya.android.http.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoInspectionRiverData implements Serializable {

    @SerializedName("id")
    public String id_online;
    public String id_local;
    public String inspection_project_id;
    public String name;
    public String description;
    public String photo;
    public String user_added;
    public String user_modified;
    public String date_added;
    public String date_modified;
    public DtoInspectionRiverData(){

    }

}
