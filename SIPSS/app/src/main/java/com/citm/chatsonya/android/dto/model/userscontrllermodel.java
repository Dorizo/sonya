package com.citm.chatsonya.android.dto.model;

import java.util.ArrayList;

public class userscontrllermodel {
    ArrayList<usersmodel> userv , userlist;

    public ArrayList<usersmodel> getUserv() {
        return userv;
    }

    public void setUserv(ArrayList<usersmodel> userv) {
        this.userv = userv;
    }

    public ArrayList<usersmodel> getUserlist() {
        return userlist;
    }

    public void setUserlist(ArrayList<usersmodel> userlist) {
        this.userlist = userlist;
    }
}
