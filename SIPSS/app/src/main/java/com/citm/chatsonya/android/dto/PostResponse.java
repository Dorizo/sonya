package com.citm.chatsonya.android.dto;

import com.citm.chatsonya.android.dto.model.Accessadmin;
import com.citm.chatsonya.android.dto.model.Post;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PostResponse {

    @SerializedName("citm")
    private ArrayList<Post> result;
    @SerializedName("adminaccess")
    private List<Accessadmin> accessadmins;

    public List<Accessadmin> getAccessadmins() {
        return accessadmins;
    }

    public void setAccessadmins(List<Accessadmin> accessadmins) {
        this.accessadmins = accessadmins;
    }

    public ArrayList<Post> getResult() {
        return result;
    }

    public void setResult(ArrayList<Post> result) {
        this.result = result;
    }
}
