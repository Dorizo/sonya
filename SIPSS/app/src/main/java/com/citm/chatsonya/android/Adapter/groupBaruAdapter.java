package com.citm.chatsonya.android.Adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.dto.model.usersmodel;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.view.group.groupBaru;
import com.shipdream.lib.android.mvc.NavigationManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;


public class groupBaruAdapter extends RecyclerView.Adapter<groupBaruAdapter.ViewHolder> {
    private List<usersmodel> datalist;
    ArrayList<usersmodel> musersmodel = new ArrayList<>();
    @Inject
    NavigationManager navigationManager;
    groupBaru context;
    DatabaseHandler db;
//    int selectedPosition=-1;
    public groupBaruAdapter(List<usersmodel> datalist, NavigationManager navigationManager, groupBaru context, DatabaseHandler db) {
        this.datalist = datalist;
        this.navigationManager = navigationManager;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_groupbaru,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.textemail.setText(datalist.get(position).getId());
        holder.textnama.setText(datalist.get(position).getName());
        final usersmodel model = datalist.get(position);
        holder.pindahgroupbaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                model.setSelected(!model.isSelected());
//                holder.cv.setBackgroundColor(model.isSelected() ? Color.BLUE : Color.WHITE);
                holder.cv.setBackgroundResource(model.isSelected()? R.color.colorPrimary:R.color.putih);
                if(model.isSelected()){
                    musersmodel.add(model);
                }else {
                    musersmodel.remove(model);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textnama, textemail;
        CircleImageView circleImageView;
        LinearLayout pindahgroupbaru;
        ImageView imgview;
        CardView cv;
        public ViewHolder(View itemView) {
            super(itemView);
            textnama = (TextView) itemView.findViewById(R.id.nama_user);
            textemail = (TextView) itemView.findViewById(R.id.lastmessage);
            circleImageView = (CircleImageView)itemView.findViewById(R.id.gambar_berita);
            pindahgroupbaru = (LinearLayout)itemView.findViewById(R.id.pindahgroupbaru);
            imgview = (ImageView)itemView.findViewById(R.id.tambah_user);
            cv = (CardView)itemView.findViewById(R.id.CVgroupBaru);
        }
    }

    public List<usersmodel> getadapterusermodel(){
        return musersmodel;
    }
}
