

package com.citm.chatsonya.android.http.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoInspectionStaRiverData implements Serializable {

    @SerializedName("id")
    public String id_online;
    public String id_local;
    public String b_1_selected;
    public String b_2_selected;
    public String b_3_selected;
    public String b_1_caption;
    public String b_2_caption;
    public String b_3_caption;


    public String c_1_1_selected;
    public String c_1_2_selected;
    public String c_1_caption;

    public String c_2_1_selected;
    public String c_2_caption;

    public String c_3_1_selected;
    public String c_3_caption;

    public String c_4_1_selected;
    public String c_4_2_selected;
    public String c_4_3_selected;
    public String c_4_caption;

    public String d_1_1_selected;
    public String d_1_2_selected;
    public String d_1_3_selected;
    public String d_1_caption;

    public String d_2_1_selected;
    public String d_2_2_selected;

    public String d_2_caption;

    public String d_3_1_selected;
    public String d_3_caption;

    public String d_4_1_selected;
    public String d_4_2_selected;
    public String d_4_caption;

    public String d_5_1_selected;
    public String d_5_2_selected;
    public String d_5_caption;

    public String d_6_1_selected;
    public String d_6_2_selected;
    public String d_6_caption;

    public String d_7_1_selected;
    public String d_7_caption;

    public String d_8_1_selected;
    public String d_8_caption;
    public String e_1_1_selected;
    public String e_1_2_selected;
    public String e_1_3_selected;
    public String e_1_4_caption;
    public String e_1_caption;




    public String f_caption;

    public String sketsa;
    public String user_added;
    public String user_modified;
    public String date_added;
    public String date_modified;
    public DtoInspectionStaRiverData(){

    }

}
