package com.citm.chatsonya.android.service.internal.database;

import com.orm.SugarRecord;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Catur on 10/29/2016.
 */


@Getter
@Setter
public class DbAsyncTask extends SugarRecord {

    String name;
    String class_name;
    String run;


    public DbAsyncTask() {
    }

    public DbAsyncTask(
            String name,
            String class_name,
            String run
    ) {
        if (name != null)
            setName(name);
        else
            setName("");
        if (class_name != null)
            setClass_name(class_name);
        else
            setClass_name("");
        if (run != null)
            setRun(run);
        else
            setRun("0");
    }

    public void edit(
            String name,
            String class_name,
            String run
    ) {
        if (name != null)
            setName(name);
        else
            setName("");
        if (class_name != null)
            setClass_name(class_name);
        else
            setClass_name("");
        if (run != null)
            setRun(run);
        else
            setRun("0");
    }
}

