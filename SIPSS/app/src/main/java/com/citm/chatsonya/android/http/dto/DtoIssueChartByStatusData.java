

package com.citm.chatsonya.android.http.dto;


import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DtoIssueChartByStatusData extends DtoParent implements Serializable {


    public String planning;
    public String on_progress;
    public String finished;
    public String total;
}
