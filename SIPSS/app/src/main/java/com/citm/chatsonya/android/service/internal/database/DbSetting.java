package com.citm.chatsonya.android.service.internal.database;

import com.orm.SugarRecord;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Catur on 10/29/2016.
 */


@Getter
@Setter
public class DbSetting extends SugarRecord {
    String var;
    String value;
    public DbSetting() {
    }

    public DbSetting(String field,
                     String value

    ) {
        if (field!=null)
            setVar(field);
        else
            setVar(field);
        if (value!=null)
            setValue(value);
        else
            setValue(value);
    }

    public void edit( String field,
                      String value


    ) {
        if (field!=null)
            setVar(field);
        else
            setVar(field);
        if (value!=null)
            setValue(value);
        else
            setValue(value);
    }
}

