package com.citm.chatsonya.android.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.citm.chatsonya.android.Manifest;
import com.citm.chatsonya.android.R;
import com.citm.chatsonya.android.controller.splashcontroller;
import com.citm.chatsonya.android.helpers.LifeCycleMonitor;
import com.citm.chatsonya.android.helpers.database.DatabaseHandler;
import com.citm.chatsonya.android.view.fragment.BeritaBaru;
import com.citm.chatsonya.android.view.fragment.Timeline;
import com.citm.chatsonya.android.view.fragment.chat;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.sdsmdg.tastytoast.TastyToast;
import com.shipdream.lib.android.mvc.MvcFragment;
import com.shipdream.lib.android.mvc.Reason;

import javax.inject.Inject;

public class viewSplashscreen extends MvcFragment<splashcontroller> {
    ViewPager viewPager;
    Toolbar mToolbar;
    private PagerAdapter pagerAdapter;
    SharedPreferences mSettings;

    DatabaseHandler dbHandler;
    @Inject
    private LifeCycleMonitor lifeCycleMonitor;

    @Override
    protected Class<splashcontroller> getControllerClass() {
        return splashcontroller.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady(View view, Bundle savedInstanceState, Reason reason) {
        lifeCycleMonitor.onCreateView(view, savedInstanceState);
        lifeCycleMonitor.onViewCreated(view, savedInstanceState);
        super.onViewReady(view, savedInstanceState, reason);

        mSettings = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
        //toolbar
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (mToolbar != null) {
            ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        }
        mToolbar.setTitle("Chat Sonya");
        setHasOptionsMenu(true);
        lifeCycleMonitor.onViewReady(view, savedInstanceState, reason);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        pagerAdapter = new PagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        SmartTabLayout viewpagerTab =(SmartTabLayout)view.findViewById(R.id.viewpagertab);
        viewpagerTab.setViewPager(viewPager);

        String s = FirebaseInstanceId.getInstance().getToken();
        dbHandler = new DatabaseHandler(getActivity().getBaseContext());
        checkAndRequestPermissions();
    }

    private boolean checkAndRequestPermissions() {
        if (ActivityCompat.checkSelfPermission(getActivity().getBaseContext(), Manifest.permission.C2D_MESSAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.C2D_MESSAGE
            }, 20);

        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.tollbar_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                // do whatever

                mSettings.edit().clear().commit();
                dbHandler.DeleteAllMessages();
                dbHandler.deleteuser();

                TastyToast.makeText(getContext(),"Selamat Tinggal" , TastyToast.LENGTH_LONG ,TastyToast.DEFAULT );
                getActivity().finish();
                return true;
            case R.id.profil:
                // do whatever
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lifeCycleMonitor.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        lifeCycleMonitor.onResume();
    }

    @Override
    protected void onReturnForeground() {
        super.onReturnForeground();
        lifeCycleMonitor.onReturnForeground();
    }

    @Override
    protected void onPushToBackStack() {
        super.onPushToBackStack();
        lifeCycleMonitor.onPushToBackStack();
    }

    @Override
    protected void onPopAway() {
        super.onPopAway();
        lifeCycleMonitor.onPopAway();
    }

    @Override
    protected void onPoppedOutToFront() {
        super.onPoppedOutToFront();
        lifeCycleMonitor.onPoppedOutToFront();
    }

    @Override
    protected void onOrientationChanged(int lastOrientation, int currentOrientation) {
        super.onOrientationChanged(lastOrientation, currentOrientation);
        lifeCycleMonitor.onOrientationChanged(lastOrientation, currentOrientation);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        lifeCycleMonitor.onDestroyView();
    }

    @Override
    public void onDestroy() {
        lifeCycleMonitor.onDestroy();
        super.onDestroy();
    }

    @Override
    public void update() {

    }

    private class PagerAdapter extends FragmentStatePagerAdapter {
        private Class<? extends MvcFragment>[] tabs = new Class[]{
                chat.class,
                Timeline.class,
                BeritaBaru.class
        };


        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Chat";
                case 1 :
                    return "Status";
                default:
                    return "News";
            }
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            try {
                Class<?> clazz = Class.forName(tabs[position].getName());
                fragment = (Fragment) clazz.newInstance();
            } catch (Exception e) {
                throw new RuntimeException("Can't instantiate fragment - "
                        + fragment.getClass().getName(), e);
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return tabs.length;
        }
    }
}
