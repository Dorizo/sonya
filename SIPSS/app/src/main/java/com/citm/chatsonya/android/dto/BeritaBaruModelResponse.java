package com.citm.chatsonya.android.dto;

import com.citm.chatsonya.android.dto.model.BeritaBaruModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BeritaBaruModelResponse {

    @SerializedName("citm")
    private ArrayList<BeritaBaruModel> result;

    public ArrayList<BeritaBaruModel> getResult() {
        return result;
    }

    public void setResult(ArrayList<BeritaBaruModel> result) {
        this.result = result;
    }
}
