package com.citm.chatsonya.android.view.chat;

import android.content.Context;
import android.util.Log;

import com.citm.chatsonya.android.dto.PostResponse;
import com.citm.chatsonya.android.dto.UserResponse;
import com.citm.chatsonya.android.dto.model.userscontrllermodel;
import com.citm.chatsonya.android.factory.RetrofitInstance;
import com.citm.chatsonya.android.http.AllDataService;
import com.google.gson.Gson;
import com.shipdream.lib.android.mvc.FragmentController;
import com.shipdream.lib.android.mvc.UiView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class UserController extends FragmentController<userscontrllermodel, UiView>{
    Context context;
    AllDataService service = RetrofitInstance.RetrofitInstance().create(AllDataService.class);
    @Override
    public Class<userscontrllermodel> modelType() {
        return userscontrllermodel.class;
    }
    public void retrofituser(String key , String serach){
        Call<UserResponse> call = service.user("Bearer "+key , serach);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()){
                    getModel().setUserv(response.body().getResult());
                    view.update();
                }else {
                    Gson gs = new Gson();
                    Log.e(TAG, "error onResponse: "+ gs.toJson(response.errorBody()));

                }

            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

            }
        });
//        Call<UserResponse> call = service.user("Bearer "+mSettings.getString("key","missing"));

    }


    public void adduser(String key , String user_two){
        Call<PostResponse> call = service.adduser("Bearer "+key , user_two);
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                Log.e(TAG, "onResponse: "+response );

            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );

            }
        });


    }
}
